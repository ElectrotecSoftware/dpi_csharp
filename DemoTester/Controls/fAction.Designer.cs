﻿namespace TestDPI1
{
    partial class fAction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fAction));
            this.uscActions1 = new TestDPI1.uscActions();
            this.SuspendLayout();
            // 
            // uscActions1
            // 
            this.uscActions1.Location = new System.Drawing.Point(0, 0);
            this.uscActions1.MChannel = DPI1.ChannelNum.Two;
            this.uscActions1.MSelectedNode = null;
            this.uscActions1.Name = "uscActions1";
            this.uscActions1.Size = new System.Drawing.Size(297, 385);
            this.uscActions1.TabIndex = 0;
            // 
            // fAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 384);
            this.Controls.Add(this.uscActions1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fAction";
            this.ShowInTaskbar = false;
            this.Text = "Node Properties";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private uscActions uscActions1;
    }
}