﻿namespace TestDPI1
{
    partial class uscNetDistribution
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uscNetDistribution));
            this.sS = new System.Windows.Forms.StatusStrip();
            this.tsSelected = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsDetected = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsConfigured = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSOpenSessions = new System.Windows.Forms.ToolStripStatusLabel();
            this.tS = new System.Windows.Forms.ToolStrip();
            this.bChannel1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bChannel2 = new System.Windows.Forms.ToolStripButton();
            this.tlpC1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbChannel = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.sS.SuspendLayout();
            this.tS.SuspendLayout();
            this.gbChannel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sS
            // 
            this.sS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSelected,
            this.tsDetected,
            this.tsConfigured,
            this.tSVersion,
            this.tSOpenSessions});
            this.sS.Location = new System.Drawing.Point(0, 399);
            this.sS.Name = "sS";
            this.sS.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.sS.Size = new System.Drawing.Size(905, 22);
            this.sS.TabIndex = 1;
            this.sS.Text = "sS";
            // 
            // tsSelected
            // 
            this.tsSelected.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.tsSelected.Name = "tsSelected";
            this.tsSelected.Size = new System.Drawing.Size(112, 17);
            this.tsSelected.Text = "Selected Node: None";
            // 
            // tsDetected
            // 
            this.tsDetected.Margin = new System.Windows.Forms.Padding(5, 3, 5, 2);
            this.tsDetected.Name = "tsDetected";
            this.tsDetected.Size = new System.Drawing.Size(116, 17);
            this.tsDetected.Text = "Detected Nodes: None";
            // 
            // tsConfigured
            // 
            this.tsConfigured.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.tsConfigured.Margin = new System.Windows.Forms.Padding(5, 3, 5, 2);
            this.tsConfigured.Name = "tsConfigured";
            this.tsConfigured.Size = new System.Drawing.Size(129, 17);
            this.tsConfigured.Text = "Configured Nodes: None";
            // 
            // tSVersion
            // 
            this.tSVersion.Name = "tSVersion";
            this.tSVersion.Size = new System.Drawing.Size(46, 17);
            this.tSVersion.Text = "Version:";
            // 
            // tSOpenSessions
            // 
            this.tSOpenSessions.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.tSOpenSessions.Margin = new System.Windows.Forms.Padding(5, 3, 5, 2);
            this.tSOpenSessions.Name = "tSOpenSessions";
            this.tSOpenSessions.Size = new System.Drawing.Size(74, 17);
            this.tSOpenSessions.Text = "Open sesion:";
            // 
            // tS
            // 
            this.tS.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bChannel1,
            this.toolStripSeparator1,
            this.bChannel2});
            this.tS.Location = new System.Drawing.Point(0, 0);
            this.tS.Name = "tS";
            this.tS.Size = new System.Drawing.Size(905, 25);
            this.tS.TabIndex = 2;
            this.tS.Text = "tS";
            this.tS.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tS_ItemClicked);
            // 
            // bChannel1
            // 
            this.bChannel1.Checked = true;
            this.bChannel1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bChannel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bChannel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bChannel1.Image = ((System.Drawing.Image)(resources.GetObject("bChannel1.Image")));
            this.bChannel1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bChannel1.Name = "bChannel1";
            this.bChannel1.Size = new System.Drawing.Size(66, 22);
            this.bChannel1.Text = "Channel 1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bChannel2
            // 
            this.bChannel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bChannel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bChannel2.Image = ((System.Drawing.Image)(resources.GetObject("bChannel2.Image")));
            this.bChannel2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bChannel2.Name = "bChannel2";
            this.bChannel2.Size = new System.Drawing.Size(66, 22);
            this.bChannel2.Text = "Channel 2";
            // 
            // tlpC1
            // 
            this.tlpC1.AutoScroll = true;
            this.tlpC1.AutoSize = true;
            this.tlpC1.BackColor = System.Drawing.Color.LightGray;
            this.tlpC1.CausesValidation = false;
            this.tlpC1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlpC1.ColumnCount = 20;
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000926F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000927F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.000427F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.998886F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.998886F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.9986F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.9986F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.9986F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.9986F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.9986F));
            this.tlpC1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.9986F));
            this.tlpC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpC1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tlpC1.Location = new System.Drawing.Point(0, 25);
            this.tlpC1.Margin = new System.Windows.Forms.Padding(0);
            this.tlpC1.Name = "tlpC1";
            this.tlpC1.RowCount = 13;
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692308F));
            this.tlpC1.Size = new System.Drawing.Size(782, 374);
            this.tlpC1.TabIndex = 0;
            // 
            // gbChannel
            // 
            this.gbChannel.Controls.Add(this.tableLayoutPanel1);
            this.gbChannel.Dock = System.Windows.Forms.DockStyle.Right;
            this.gbChannel.Location = new System.Drawing.Point(782, 25);
            this.gbChannel.Name = "gbChannel";
            this.gbChannel.Size = new System.Drawing.Size(123, 374);
            this.gbChannel.TabIndex = 4;
            this.gbChannel.TabStop = false;
            this.gbChannel.Text = "Node types";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label12, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label11, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090392F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.093127F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.093127F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09009F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(117, 355);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Yellow;
            this.panel5.Location = new System.Drawing.Point(3, 99);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(29, 26);
            this.panel5.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Interface";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "LC Series";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Cyan;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 67);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(29, 26);
            this.panel4.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkGreen;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 35);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(29, 26);
            this.panel3.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "DP Series";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 26);
            this.label6.TabIndex = 1;
            this.label6.Text = "DP Series Zone Display";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LimeGreen;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(29, 26);
            this.panel2.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 227);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(29, 26);
            this.panel8.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(38, 224);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 32);
            this.label15.TabIndex = 12;
            this.label15.Text = "Disconnected";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Red;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 195);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(29, 26);
            this.panel6.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(38, 192);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 32);
            this.label12.TabIndex = 11;
            this.label12.Text = "Error";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Blue;
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 163);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(29, 26);
            this.panel7.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(38, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 32);
            this.label11.TabIndex = 10;
            this.label11.Text = "Local Mode";
            // 
            // uscNetDistribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpC1);
            this.Controls.Add(this.gbChannel);
            this.Controls.Add(this.tS);
            this.Controls.Add(this.sS);
            this.Name = "uscNetDistribution";
            this.Size = new System.Drawing.Size(905, 421);
            this.Load += new System.EventHandler(this.uscNetDistribution_Load);
            this.sS.ResumeLayout(false);
            this.sS.PerformLayout();
            this.tS.ResumeLayout(false);
            this.tS.PerformLayout();
            this.gbChannel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip sS;
        private System.Windows.Forms.ToolStrip tS;
        private System.Windows.Forms.ToolStripButton bChannel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel tsSelected;
        private System.Windows.Forms.ToolStripStatusLabel tsDetected;
        private System.Windows.Forms.ToolStripButton bChannel2;
        public System.Windows.Forms.TableLayoutPanel tlpC1;
        private System.Windows.Forms.GroupBox gbChannel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripStatusLabel tSVersion;
        private System.Windows.Forms.ToolStripStatusLabel tSOpenSessions;
        private System.Windows.Forms.ToolStripStatusLabel tsConfigured;
    }
}
