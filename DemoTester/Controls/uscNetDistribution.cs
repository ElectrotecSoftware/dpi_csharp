﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DPI1;
namespace TestDPI1
{
    public partial class uscNetDistribution : UserControl
    {
        DPI1.DPI1 data = new DPI1.DPI1();
        DP mSelectedNode = null;
        ChannelNum mChannel=ChannelNum.One;
        int channel = 0;
        public uscNetDistribution()
        {
            this.SuspendLayout();
            InitializeComponent();
            this.DoubleBuffered = true;
            this.ResizeRedraw = false;
            this.ResumeLayout();

        }
        public event EventHandler Node_Clicked;
        public ChannelNum MChannel { get { return mChannel; } set { mChannel = value; channel = (mChannel == ChannelNum.One) ? 0 : 1; } }
        public DP MSelectedNode { get { return mSelectedNode; } }

        public uscNetDistribution(DPI1.DPI1 _data)
            : this()
        {
            UpdateData(_data);
        }
        public void UpdateData(DPI1.DPI1 _data)
        {
            data = _data;
            LoadTable(data);
        }

        public void LoadTable(DPI1.DPI1 net)
        {


            if (net.NetDistribution.Count > 0)
            {
                tlpC1.Visible = false;
                this.tlpC1.SuspendLayout();
                this.tlpC1.Controls.Clear();
                tsDetected.Text = "Detected Nodes: " + net.NetDistribution[channel].ListOfDevices.FindAll((DP dp) => {return dp.State == DPI1.Status.Normal; }).Count.ToString();
                tsConfigured.Text="Configured Nodes: " + net.NetDistribution[channel].ListOfDevices.Count.ToString();
                tSVersion.Text = "Version: " + net.Version.ToString(); ;
                tSOpenSessions.Text = "Open Sesions: " + net.OpenSessions.ToString();
                foreach (DP dp in net.NetDistribution[channel].ListOfDevices)
                {

                    Label txtNode = new Label();
                    txtNode.Tag = dp;
                    txtNode.Text = txtNode.Name = dp.id.ToString() +Environment.NewLine+ dp.DpType.ToString();
                    txtNode.Tag = dp;
                    txtNode.Height = 30;
                    txtNode.Font = new Font(FontFamily.GenericSansSerif, 7);
                    txtNode.Anchor = AnchorStyles.Top;

                    txtNode.Click += new System.EventHandler(this.OnNodeClicked);
                    switch (dp.DpType)
                    {
                        case DPI1.DisplayType.DPA1:
                        case DPI1.DisplayType.DPM1: 
                        case DPI1.DisplayType.DPA2: txtNode.BackColor = System.Drawing.Color.LimeGreen; break;
                        case DPI1.DisplayType.DPAZ1: 
                        case DPI1.DisplayType.DPMZ1: txtNode.BackColor = System.Drawing.Color.DarkGreen; break;
                        case DPI1.DisplayType.LC1: txtNode.BackColor = System.Drawing.Color.Cyan; break;
                        case DPI1.DisplayType.LCI: 
                        case DPI1.DisplayType.DPW1: txtNode.BackColor = System.Drawing.Color.Yellow; break;
                        case DPI1.DisplayType.Unknown: txtNode.BackColor = System.Drawing.Color.WhiteSmoke; break;
                    }
                    switch (dp.State)
                    {
                        case DPI1.Status.Normal: break;
                        case DPI1.Status.Program: break;
                        case DPI1.Status.Init: break;
                        case DPI1.Status.Local: txtNode.BackColor = System.Drawing.Color.Blue; break;
                        case DPI1.Status.Unknown: txtNode.BackColor = System.Drawing.Color.WhiteSmoke; break;
                        case DPI1.Status.Error: txtNode.BackColor = System.Drawing.Color.Red; break;

                    }

                    int column = (Convert.ToInt32(dp.id) - 1) % tlpC1.ColumnCount;
                    int row = (Convert.ToInt32(dp.id) - 1) / tlpC1.ColumnCount;
                    tlpC1.Controls.Add(txtNode, column, row);

                }

                this.tlpC1.ResumeLayout();
                this.tlpC1.Visible = true;

            }


        }

        private void tS_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "bChannel1": MChannel = ChannelNum.One; bChannel1.Checked = true; bChannel2.Checked = false; break;
                case "bChannel2": MChannel = ChannelNum.Two; bChannel1.Checked = false; bChannel2.Checked = true; break;
            }
            LoadTable(data);
        }

        private void uscNetDistribution_Load(object sender, EventArgs e)
        {
            LoadTable(data);
        }


       
        protected virtual void OnNodeClicked(object sender,EventArgs e)
        {
            Label mSelectedLabel = (Label)sender;
            mSelectedNode = (DP)mSelectedLabel.Tag;
            tsSelected.Text = "Selected Node: " + mSelectedLabel.Text;
            
            int index = this.tlpC1.Controls.GetChildIndex(mSelectedLabel);

            if (this.Node_Clicked != null) this.Node_Clicked(this, e);
            tsSelected.Text = "";
            
        }   

    }
}


