﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DPI1;

namespace TestDPI1
{
    public partial class fAction : Form
    {
        public DPI1.DPI1 Mdisplay { set { uscActions1.display = value; } get { return uscActions1.display; } }

        public DP MSelectedNode { set { uscActions1.MSelectedNode = value; this.Text = value.ToString(); } get { return uscActions1.MSelectedNode; } }
        public ChannelNum MChannel { set { uscActions1.MChannel = value; } get { return uscActions1.MChannel; } }
        public event EventHandler SendToNode_Click;

        public fAction()
        {
            InitializeComponent();
            this.uscActions1.bPrint.Click+= new EventHandler(Send);
        }
        
        public fAction(DPI1.DPI1 _display):this(){
            
            this.Mdisplay= _display;
        }
        public fAction(DPI1.DPI1 _display, DP _selectedNode, ChannelNum _Channel)
            : this(_display)
        {
            this.MSelectedNode=_selectedNode;
            this.Text = _selectedNode.ToString();
            this.MChannel = _Channel;
           
        }
        protected void Send(object sender, EventArgs e)
        {
           if(SendToNode_Click!=null) this.SendToNode_Click(this, e);
           this.DialogResult = DialogResult.OK;
        }
        
    }
}
