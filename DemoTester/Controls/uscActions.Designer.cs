﻿namespace TestDPI1
{
    partial class uscActions
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbPrint = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.ArrowsComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbColors = new System.Windows.Forms.GroupBox();
            this.None = new System.Windows.Forms.RadioButton();
            this.White = new System.Windows.Forms.RadioButton();
            this.Pink = new System.Windows.Forms.RadioButton();
            this.Yellow = new System.Windows.Forms.RadioButton();
            this.Blue = new System.Windows.Forms.RadioButton();
            this.LightBlue = new System.Windows.Forms.RadioButton();
            this.Green = new System.Windows.Forms.RadioButton();
            this.Red = new System.Windows.Forms.RadioButton();
            this.blinkComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cBoxKeyEnabled = new System.Windows.Forms.CheckBox();
            this.beepComboBox = new System.Windows.Forms.ComboBox();
            this.gbTransmit = new System.Windows.Forms.GroupBox();
            this.bPrint = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gbPrint.SuspendLayout();
            this.gbColors.SuspendLayout();
            this.gbTransmit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbPrint
            // 
            this.gbPrint.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbPrint.Controls.Add(this.label26);
            this.gbPrint.Controls.Add(this.ArrowsComboBox);
            this.gbPrint.Controls.Add(this.label2);
            this.gbPrint.Controls.Add(this.gbColors);
            this.gbPrint.Controls.Add(this.blinkComboBox);
            this.gbPrint.Controls.Add(this.label13);
            this.gbPrint.Controls.Add(this.txtText);
            this.gbPrint.Controls.Add(this.label14);
            this.gbPrint.Controls.Add(this.cBoxKeyEnabled);
            this.gbPrint.Controls.Add(this.beepComboBox);
            this.gbPrint.Location = new System.Drawing.Point(3, 5);
            this.gbPrint.Name = "gbPrint";
            this.gbPrint.Size = new System.Drawing.Size(288, 303);
            this.gbPrint.TabIndex = 71;
            this.gbPrint.TabStop = false;
            this.gbPrint.Text = "Node options";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(154, 246);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 14);
            this.label26.TabIndex = 69;
            this.label26.Text = "Arrows";
            // 
            // ArrowsComboBox
            // 
            this.ArrowsComboBox.Enabled = false;
            this.ArrowsComboBox.FormattingEnabled = true;
            this.ArrowsComboBox.Location = new System.Drawing.Point(204, 244);
            this.ArrowsComboBox.Name = "ArrowsComboBox";
            this.ArrowsComboBox.Size = new System.Drawing.Size(68, 21);
            this.ArrowsComboBox.TabIndex = 70;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 14);
            this.label2.TabIndex = 68;
            this.label2.Text = "Blink";
            // 
            // gbColors
            // 
            this.gbColors.BackColor = System.Drawing.Color.White;
            this.gbColors.Controls.Add(this.None);
            this.gbColors.Controls.Add(this.White);
            this.gbColors.Controls.Add(this.Pink);
            this.gbColors.Controls.Add(this.Yellow);
            this.gbColors.Controls.Add(this.Blue);
            this.gbColors.Controls.Add(this.LightBlue);
            this.gbColors.Controls.Add(this.Green);
            this.gbColors.Controls.Add(this.Red);
            this.gbColors.Location = new System.Drawing.Point(17, 29);
            this.gbColors.Name = "gbColors";
            this.gbColors.Size = new System.Drawing.Size(256, 171);
            this.gbColors.TabIndex = 67;
            this.gbColors.TabStop = false;
            this.gbColors.Text = "LED Colors";
            // 
            // None
            // 
            this.None.AutoSize = true;
            this.None.Image = global::TestDPI1.Properties.Resources.b_button;
            this.None.Location = new System.Drawing.Point(142, 15);
            this.None.Margin = new System.Windows.Forms.Padding(0);
            this.None.MinimumSize = new System.Drawing.Size(0, 20);
            this.None.Name = "None";
            this.None.Size = new System.Drawing.Size(75, 24);
            this.None.TabIndex = 7;
            this.None.Text = "None";
            this.None.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.None.UseVisualStyleBackColor = true;
            // 
            // White
            // 
            this.White.AutoSize = true;
            this.White.Image = global::TestDPI1.Properties.Resources.white_button;
            this.White.Location = new System.Drawing.Point(142, 89);
            this.White.Margin = new System.Windows.Forms.Padding(0);
            this.White.MinimumSize = new System.Drawing.Size(0, 20);
            this.White.Name = "White";
            this.White.Size = new System.Drawing.Size(77, 24);
            this.White.TabIndex = 6;
            this.White.Text = "White";
            this.White.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.White.UseVisualStyleBackColor = true;
            // 
            // Pink
            // 
            this.Pink.AutoSize = true;
            this.Pink.Image = global::TestDPI1.Properties.Resources.pink_button;
            this.Pink.Location = new System.Drawing.Point(142, 55);
            this.Pink.Margin = new System.Windows.Forms.Padding(0);
            this.Pink.MinimumSize = new System.Drawing.Size(0, 20);
            this.Pink.Name = "Pink";
            this.Pink.Size = new System.Drawing.Size(70, 24);
            this.Pink.TabIndex = 5;
            this.Pink.Text = "Pink";
            this.Pink.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Pink.UseVisualStyleBackColor = true;
            // 
            // Yellow
            // 
            this.Yellow.AutoSize = true;
            this.Yellow.Image = global::TestDPI1.Properties.Resources.yellow_button;
            this.Yellow.Location = new System.Drawing.Point(142, 131);
            this.Yellow.Margin = new System.Windows.Forms.Padding(0);
            this.Yellow.MinimumSize = new System.Drawing.Size(0, 20);
            this.Yellow.Name = "Yellow";
            this.Yellow.Size = new System.Drawing.Size(80, 24);
            this.Yellow.TabIndex = 4;
            this.Yellow.Text = "Yellow";
            this.Yellow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Yellow.UseVisualStyleBackColor = true;
            // 
            // Blue
            // 
            this.Blue.AutoSize = true;
            this.Blue.Image = global::TestDPI1.Properties.Resources.blue_button1;
            this.Blue.Location = new System.Drawing.Point(35, 128);
            this.Blue.Margin = new System.Windows.Forms.Padding(0);
            this.Blue.MinimumSize = new System.Drawing.Size(0, 20);
            this.Blue.Name = "Blue";
            this.Blue.Size = new System.Drawing.Size(70, 24);
            this.Blue.TabIndex = 3;
            this.Blue.Text = "Blue";
            this.Blue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Blue.UseVisualStyleBackColor = true;
            // 
            // LightBlue
            // 
            this.LightBlue.AutoSize = true;
            this.LightBlue.Image = global::TestDPI1.Properties.Resources.turquoise_button;
            this.LightBlue.Location = new System.Drawing.Point(35, 88);
            this.LightBlue.Margin = new System.Windows.Forms.Padding(0);
            this.LightBlue.MinimumSize = new System.Drawing.Size(0, 20);
            this.LightBlue.Name = "LightBlue";
            this.LightBlue.Size = new System.Drawing.Size(96, 24);
            this.LightBlue.TabIndex = 2;
            this.LightBlue.Text = "Light Blue";
            this.LightBlue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.LightBlue.UseVisualStyleBackColor = true;
            // 
            // Green
            // 
            this.Green.AutoSize = true;
            this.Green.Image = global::TestDPI1.Properties.Resources.green_button1;
            this.Green.Location = new System.Drawing.Point(35, 55);
            this.Green.Margin = new System.Windows.Forms.Padding(0);
            this.Green.MinimumSize = new System.Drawing.Size(0, 20);
            this.Green.Name = "Green";
            this.Green.Size = new System.Drawing.Size(78, 24);
            this.Green.TabIndex = 1;
            this.Green.Text = "Green";
            this.Green.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Green.UseVisualStyleBackColor = true;
            // 
            // Red
            // 
            this.Red.AutoSize = true;
            this.Red.Checked = true;
            this.Red.Image = global::TestDPI1.Properties.Resources.red_button1;
            this.Red.Location = new System.Drawing.Point(35, 15);
            this.Red.Margin = new System.Windows.Forms.Padding(0);
            this.Red.MinimumSize = new System.Drawing.Size(0, 20);
            this.Red.Name = "Red";
            this.Red.Size = new System.Drawing.Size(69, 24);
            this.Red.TabIndex = 0;
            this.Red.TabStop = true;
            this.Red.Text = "Red";
            this.Red.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Red.UseVisualStyleBackColor = true;
            // 
            // blinkComboBox
            // 
            this.blinkComboBox.FormattingEnabled = true;
            this.blinkComboBox.Location = new System.Drawing.Point(54, 209);
            this.blinkComboBox.Name = "blinkComboBox";
            this.blinkComboBox.Size = new System.Drawing.Size(68, 21);
            this.blinkComboBox.TabIndex = 57;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 275);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 16);
            this.label13.TabIndex = 61;
            this.label13.Text = "Text";
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(56, 271);
            this.txtText.MaxLength = 30;
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(217, 20);
            this.txtText.TabIndex = 54;
            this.txtText.Text = "Electrotec Software y Control";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(163, 211);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 14);
            this.label14.TabIndex = 59;
            this.label14.Text = "Beep";
            // 
            // cBoxKeyEnabled
            // 
            this.cBoxKeyEnabled.AutoSize = true;
            this.cBoxKeyEnabled.Location = new System.Drawing.Point(20, 246);
            this.cBoxKeyEnabled.Name = "cBoxKeyEnabled";
            this.cBoxKeyEnabled.Size = new System.Drawing.Size(114, 17);
            this.cBoxKeyEnabled.TabIndex = 58;
            this.cBoxKeyEnabled.Text = "KeySoundEnabled";
            this.cBoxKeyEnabled.UseVisualStyleBackColor = true;
            // 
            // beepComboBox
            // 
            this.beepComboBox.FormattingEnabled = true;
            this.beepComboBox.Location = new System.Drawing.Point(204, 209);
            this.beepComboBox.Name = "beepComboBox";
            this.beepComboBox.Size = new System.Drawing.Size(68, 21);
            this.beepComboBox.TabIndex = 60;
            // 
            // gbTransmit
            // 
            this.gbTransmit.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbTransmit.Controls.Add(this.pictureBox1);
            this.gbTransmit.Controls.Add(this.bPrint);
            this.gbTransmit.Location = new System.Drawing.Point(3, 312);
            this.gbTransmit.Name = "gbTransmit";
            this.gbTransmit.Size = new System.Drawing.Size(288, 70);
            this.gbTransmit.TabIndex = 72;
            this.gbTransmit.TabStop = false;
            this.gbTransmit.Text = "Transmit";
            // 
            // bPrint
            // 
            this.bPrint.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bPrint.Image = global::TestDPI1.Properties.Resources.green_arrow_down;
            this.bPrint.Location = new System.Drawing.Point(145, 19);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(128, 43);
            this.bPrint.TabIndex = 53;
            this.bPrint.Text = "Send to Node";
            this.bPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bPrint.UseVisualStyleBackColor = false;
            this.bPrint.Click += new System.EventHandler(this.bPrint_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::TestDPI1.Properties.Resources.DPA1;
            this.pictureBox1.Location = new System.Drawing.Point(3, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(72, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 73;
            this.pictureBox1.TabStop = false;
            // 
            // uscActions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbTransmit);
            this.Controls.Add(this.gbPrint);
            this.Name = "uscActions";
            this.Size = new System.Drawing.Size(295, 387);
            this.gbPrint.ResumeLayout(false);
            this.gbPrint.PerformLayout();
            this.gbColors.ResumeLayout(false);
            this.gbColors.PerformLayout();
            this.gbTransmit.ResumeLayout(false);
            this.gbTransmit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbPrint;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox ArrowsComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbColors;
        private System.Windows.Forms.RadioButton None;
        private System.Windows.Forms.RadioButton White;
        private System.Windows.Forms.RadioButton Pink;
        private System.Windows.Forms.RadioButton Yellow;
        private System.Windows.Forms.RadioButton Blue;
        private System.Windows.Forms.RadioButton LightBlue;
        private System.Windows.Forms.RadioButton Green;
        private System.Windows.Forms.RadioButton Red;
        private System.Windows.Forms.ComboBox blinkComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cBoxKeyEnabled;
        private System.Windows.Forms.ComboBox beepComboBox;
        private System.Windows.Forms.GroupBox gbTransmit;
        public System.Windows.Forms.Button bPrint;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
