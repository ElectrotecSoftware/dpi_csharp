﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DPI1;

namespace TestDPI1
{
    public partial class uscActions : UserControl
    {
        public DPI1.DPI1 display=new DPI1.DPI1();
        DP mSelectedNode = null;
        ChannelNum mChannel = ChannelNum.Two;


        public uscActions()
        {
            InitializeComponent();

            blinkComboBox.DataSource = Enum.GetValues(typeof(DPI1.Blinker));
            blinkComboBox.SelectedItem = DPI1.Blinker.NoBlink;
            beepComboBox.DataSource = Enum.GetValues(typeof(DPI1.BeepType));
            beepComboBox.SelectedItem = DPI1.BeepType.OneBeep;
            ArrowsComboBox.DataSource = Enum.GetValues(typeof(DPI1.ArrowsPos));
            ArrowsComboBox.SelectedItem = DPI1.ArrowsPos.Right;
        }
        public uscActions(DPI1.DPI1 _display):this(){
            display=_display;      
        }
        public uscActions(DPI1.DPI1 _display,DP _selectedNode,ChannelNum _Channel):this(_display){
            MSelectedNode = _selectedNode;
            MChannel = _Channel;
            ShowNodeProperties();

        }
        public ChannelNum MChannel { get { return mChannel; } set { mChannel = value; } }
        public DP MSelectedNode { get { return mSelectedNode; } set { mSelectedNode = value; ShowNodeProperties(); } }

        private void ShowNodeProperties() {
            if (mSelectedNode != null)
            {
                switch (mSelectedNode.DpType)
                {
                    case DisplayType.DPA2:
                        ArrowsComboBox.Enabled = true;
                        pictureBox1.Image = TestDPI1.Properties.Resources.DPA1;
                        White.Enabled = Green.Enabled = Pink.Enabled = LightBlue.Enabled = Blue.Enabled = Yellow.Enabled = true;
                        gbPrint.Enabled = gbTransmit.Enabled = true;
                        cBoxKeyEnabled.Enabled = beepComboBox.Enabled = true;
                        txtText.Enabled = true;
                        break;
                    case DisplayType.DPA1:
                    case DisplayType.DPM1:
                        ArrowsComboBox.Enabled = false;
                        pictureBox1.Image = TestDPI1.Properties.Resources.DPA1;
                        White.Enabled = Green.Enabled = Pink.Enabled = LightBlue.Enabled = Blue.Enabled = Yellow.Enabled = true;
                        gbPrint.Enabled = gbTransmit.Enabled = true;
                        cBoxKeyEnabled.Enabled = beepComboBox.Enabled = true;
                        txtText.Enabled = true;
                    break;
                    case DisplayType.DPMZ1:
                    case DisplayType.DPAZ1:
                        ArrowsComboBox.Enabled = false;
                        pictureBox1.Image = TestDPI1.Properties.Resources.DPAZ1;
                        White.Enabled = Green.Enabled = Pink.Enabled = LightBlue.Enabled = Blue.Enabled = Yellow.Enabled = true;
                        gbPrint.Enabled = gbTransmit.Enabled = true;
                        cBoxKeyEnabled.Enabled = beepComboBox.Enabled = true;
                        txtText.Enabled = true;
                        break;
                    case DisplayType.DPW1:
                    case DisplayType.LCI:
                        ArrowsComboBox.Enabled = false;
                        pictureBox1.Image = TestDPI1.Properties.Resources.DPW1;
                        White.Enabled = Green.Enabled = Pink.Enabled = LightBlue.Enabled = Blue.Enabled = Yellow.Enabled = true;
                        gbPrint.Enabled = gbTransmit.Enabled = false;
                        cBoxKeyEnabled.Enabled = beepComboBox.Enabled = true;
                        txtText.Enabled = true;

                        break;
                    case DisplayType.LC1:
                        ArrowsComboBox.Enabled = false;
                        pictureBox1.Image = TestDPI1.Properties.Resources.LC1;
                        White.Enabled= Green.Enabled=Pink.Enabled=LightBlue.Enabled=Blue.Enabled=Yellow.Enabled= false;
                        gbPrint.Enabled = gbTransmit.Enabled = true;
                        cBoxKeyEnabled.Enabled= beepComboBox.Enabled= false;
                        txtText.Enabled = false;
                        break;
                }

                
            }
            
        }

        private void bPrint_Click(object sender, EventArgs e)
        {
            try
            {   
                string node = (string)mSelectedNode.id;
                DPI1.KeySound KeySoundEnabled = KeySound.Disabled;
    
                var checkedColor = gbColors.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                if (cBoxKeyEnabled.Checked == true) KeySoundEnabled = KeySound.Enabled;
                display.Message = new DPI1.Message((DPI1.Blinker)blinkComboBox.SelectedItem
                    , (DPI1.BeepType)beepComboBox.SelectedItem
                    , (DPI1.ArrowsPos)ArrowsComboBox.SelectedItem
                    , MChannel
                    , KeySoundEnabled
                    , (DPI1.Color)Enum.Parse(typeof(DPI1.Color), checkedColor.Name)
                    , txtText.Text);

                display.SetDisplayPrint(node);      
            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Actions", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
 
            
        }
    }
}
