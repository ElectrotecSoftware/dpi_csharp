﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Threading;
using DPI1;


namespace TestDPI1
{
 

 
    public partial class fMain : Form
    {

        public DPI1.DPI1 display;
        public bool mRefresh = false;
        public string mSelectedNode = null;
        public string mErrorDesc = "";
       
   

        public fMain()
        {
            InitializeComponent();
            InitializeObjects();
            InitializeControls();
        }

 
#region Initialize
        void InitializeControls()
        {
                  
            pgGrid.SelectedObject = display;
            txtRxAlarms.Text = txtRxbuffer.Text = txtRxKeypressBuffer.Text = "";
            this.DoubleBuffered = true;
    
            ChannelComboBox.DataSource = Enum.GetValues(typeof(DPI1.ChannelNum));
            ChannelComboBox.SelectedItem = DPI1.ChannelNum.One;
            cmboBoxChannel.DataSource = Enum.GetValues(typeof(DPI1.ChannelNum));
            cmboBoxChannel.SelectedItem = DPI1.ChannelNum.One;


            uscNetDistribution2.UpdateData(display);

            tabMain.TabPages.Remove(tab2);
            tabMain.TabPages.Remove(tab3);
            tabMain.Refresh();

            switch (TestDPI1.Properties.Settings.Default.AccessLevel)
            {
                case 1:  break;
                case 2:
                        if(tabMain.TabPages.IndexOf(tab2)<0) tabMain.TabPages.Insert(2, tab2);      
                    break;
                case 3: 
                        if (tabMain.TabPages.IndexOf(tab2) < 0) tabMain.TabPages.Insert(tabMain.TabPages.Count,tab2);
                        if (tabMain.TabPages.IndexOf(tab3) < 0) tabMain.TabPages.Insert(tabMain.TabPages.Count,tab3);
                    break;
            }
            bConnected(true);
 
        }       
        private void InitializeObjects()
        {
            display = new DPI1.DPI1();
            
            tsIP.Text = display.IppAdress;
            tsPort.Text = display.Port.ToString();
            uscActions1.display = display;

            display.PrintMessageACK += new EventHandler(PrintMessageACK);
            display.ConnectedStatusChanged += new EventHandler(ConnectedStatusChanged);
            display.AlarmBuffer.Enqueued += new EventHandler(ReceivedData);
            display.KeyPressBuffer.Enqueued += new EventHandler(ReceivedData);
            display.RxSerialBuffer.Enqueued += new EventHandler(ReceivedData);
            display.rxBuffer.Enqueued += new EventHandler(ReceivedData);
            
          

        }
        private void bConnected(bool enable)
        {
            tsIP.Enabled = enable;
            tsPort.Enabled = enable;
            bConnect.Visible = enable;
            bDisconnect.Visible = !enable;

            tab2.Enabled = !enable;
            tab3.Enabled = !enable;

            tSConnected.Visible = !enable;
            tSDisConnected.Visible = enable;
            tSRefresh.Visible = !enable;
        }
#endregion

#region Connect & disconnect
        private void fMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            display.Stop();
            timerCheckData.Stop();
            display.Dispose();
            this.Dispose();
        }
        void ConnectedStatusChanged(object source, EventArgs e)
        {
            try
            {
                if ((bool)source) { SetText(txtEvents, DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss:fff") + "- Connection done"); }
                else { SetText(txtEvents, DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss:fff") + "- Disconnected"); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }
        }
        private void bDisconnect_Click(object sender, EventArgs e)
        {
            timerCheckData.Stop();
            if (display.Stop()) bConnected(true);

        }
        private void bConnect_Click(object sender, EventArgs e)
        {
            display.IppAdress = tsIP.Text;
            display.Port = Convert.ToInt32(tsPort.Text);
            InitializeControls();
            timerCheckData.Start();
            if (display.Start()) RefreshForm();
            else MessageBox.Show("Connection not done.", "Connection State", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
#endregion
        int count = 0;
#region Refresh Display Nodes Information
        private void timerCheckData_Tick(object sender, EventArgs e)
        {
            RefreshForm();
            ckbTestMode_CheckedChanged(ckbTestMode, new EventArgs());
        }

        private void RefreshForm()
        {
            ChannelComboBox_SelectedIndexChanged(this, new EventArgs());
            bConnected(!display.IsOpen);
            if((Convert.ToDouble(display.Version) / 10) > 1.7) tSload.Visible = gBProgramNodes.Visible = true;
            else tSload.Visible = gBProgramNodes.Visible = false;
            TSError.Visible=display.Error || (display.AlarmBuffer.Count>0);
            tSStatus.Text = display.StrError;
            tSStatus.Visible = display.Error;
            if (display.Error && display.StrError != mErrorDesc)
            {
                SetText(txtEvents, DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss:fff") + "- Error " + display.StrError);
                mErrorDesc = display.StrError;
            } 
            tsTime.Text = DateTime.Now.ToString("hh:mm:ss:fff");
            pgGrid.Refresh();
        }
        private void ChannelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                if (display.NetDistribution.Count > 0)
                {
                    NodeComboBox.DataSource = display.NetDistribution[ChannelComboBox.SelectedIndex].ListOfDevices;
                    uscActions1.MChannel = (ChannelNum)ChannelComboBox.SelectedItem;
                    
                }
                if (cmboBoxChannel.SelectedItem != null)  gBChangeNodeID.Enabled = true;
                else gBChangeNodeID.Enabled = false;
                    
            }
            catch (Exception ex) { SetText(txtEvents,"Selected Node: "+ ex.Message.ToString()); }
        }
     

        //PrintMessageACK event
        void PrintMessageACK(object source, EventArgs e)
        {
            try
            {
                List<PrintDisplay> a = (List<PrintDisplay>)source;
                if (a.Count > 0)
                {
                   
                    SetText(txtEvents, "----------------TransmisionBlock_Start----------------");
                    foreach (PrintDisplay l in a)  SetText(txtEvents, l.Response.ToString());
                    SetText(txtEvents, "----------------TransmisionBlock_End----------------");
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }

        }
        private void SetText(TextBox txt, string text)
        {
            if (txt.InvokeRequired) Invoke((MethodInvoker)(() => txt.Text = txt.Text + Environment.NewLine + text));
            else
            {
                txt.Text = txt.Text + Environment.NewLine + text;
                txt.SelectionStart = txt.Text.Length;
                txt.ScrollToCaret();
            }
        }
        private void SetColor(Panel txt, System.Drawing.Color color)
        {
            if (txt.InvokeRequired) Invoke((MethodInvoker)(() => txt.BackColor = color));
            else txt.BackColor = color;
        }

        //Receive data event
        void ReceivedData(object source, EventArgs e)
        {
            try
            {
                if (display.rxBuffer.Count > 0) { SetText(txtRxbuffer, display.rxBuffer.Dequeue().ToString()); }
                if (display.AlarmBuffer.Count > 0) SetText(this.txtRxAlarms, display.AlarmBuffer.Dequeue().ToString());
                if (display.RxSerialBuffer.Count > 0)
                {
                    Serial data = display.RxSerialBuffer.Dequeue();
                    SetText(this.txtSerialBuffer, data.ToString());
                    checkRXRs232(data);
                }
                if (display.KeyPressBuffer.Count > 0)
                {
                    KeyPress data = display.KeyPressBuffer.Dequeue();
                    SetText(this.txtRxKeypressBuffer,
                    data.ToString());
                    checkKeyPress(data);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }
        }

    
        #endregion

        #region ACTIONS

   
        private void bSendAll_Click(object sender, EventArgs e)
        {
            try
            {
                string node = (string)((DP)NodeComboBox.SelectedItem).id;
                DPI1.KeySound KeySoundEnabled = KeySound.Disabled;
                KeySoundEnabled = KeySound.Disabled;

                    List<DPI1.PrintDisplay> p = new List<DPI1.PrintDisplay>();
                    for (int i = 1; i <= 4; i++)
                    {
         
                        DPI1.Message msg = new DPI1.Message(DPI1.Blinker.NoBlink, BeepType.OneBeep, (DPI1.ChannelNum)ChannelComboBox.SelectedItem
                        , KeySoundEnabled, DPI1.Color.None, i.ToString());

                        p.Add(new DPI1.PrintDisplay(i.ToString("000"), msg));
                        Application.DoEvents();
                    }
                    display.SetDisplayPrintACK(p);
               
            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Actions", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }

        }
        private void NodeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cBoxBroadcast.Checked == true)
            {
                uscActions1.MSelectedNode = new DP("252", DisplayType.DPA1);
            }
            else uscActions1.MSelectedNode = (DP)NodeComboBox.SelectedItem;


        }
        private void bSerial_Click(object sender, EventArgs e)
        {
            display.GetVersion();
            txtVersion.Text = display.Version;
            mRefresh = true;
        }
        private void bOpenSessions_Click(object sender, EventArgs e)
        {
            display.GetOpenSessions();
            txtOpenSessions.Text = display.OpenSessions.ToString();
        }
        private void bNetDistribution_Click(object sender, EventArgs e)
        {
            display.GetNetDistribution();
            uscNetDistribution2.UpdateData(display);
            int total=0;
            foreach (Channel c in display.NetDistribution) total = total + c.NumDisplays;
            txtTotalNodes.Text = total.ToString();
        }
        private void ckbTestMode_CheckedChanged(object sender, EventArgs e)
        {
            count++;
            foreach (Channel c in display.NetDistribution)
            {
                foreach (DP d in c.ListOfDevices)
                {
                    display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.NoBeep, DPI1.ArrowsPos.LeftUp, c.ChannelNum, DPI1.KeySound.Disabled, DPI1.Color.Red, count.ToString("000"));
                    display.SetDisplayPrint(d.id);                  
                }
            }
        }

     
        private void bClr_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Name)
            {
                case "bClrSerialBuffer": txtSerialBuffer.Text = ""; break;
                case "bClrAlarms": txtRxAlarms.Text = ""; break;
                case "bClrKeypress": txtRxKeypressBuffer.Text = ""; break;
                case "bClrRxBuffer": txtRxbuffer.Text = ""; break;
                case "bClrEvents": txtEvents.Text = ""; break;
            }
        }
        #endregion

        #region TEST MODE FUNCTIONS
        private void checkRXRs232(Serial RxSerial)
        {
            if (!ckbTestMode.Checked) return;
            if(RxSerial.BarCode.Contains("AbrirZona"))
            { //CASE: AbrirZona Mario
                    display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.OneBeep,  DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.Green, "MARIO");
                    display.SetDisplayPrint("001");
                    display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.DoubleShortBeep, DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.None, " ");
                    display.SetDisplayPrint("002");
                    display.SetDisplayPrint("003");
                    display.SetDisplayPrint("004");
            }else if(RxSerial.BarCode.Contains( "CerrarZona"))
            {   //CASE: CerrarZona Mario
                    display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.NoBeep,  DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.White, "CERRADA");
                    display.SetDisplayPrint("001");
                    display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.DoubleShortBeep,  DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.None, " ");
                    display.SetDisplayPrint("002");
                    display.SetDisplayPrint("003");
                    display.SetDisplayPrint("004");
            } else  if(RxSerial.BarCode.Contains("PED13-0004"))
            {
                //CASE: PED13-0004
                    display.Message = new DPI1.Message(DPI1.Blinker.Normal, DPI1.BeepType.OneBeep, DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.Blue, "1");
                    display.SetDisplayPrint("002");
                    display.Message = new DPI1.Message(DPI1.Blinker.Normal, DPI1.BeepType.OneBeep, DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.Blue, "23");
                    display.SetDisplayPrint("004");
            }else  if(RxSerial.BarCode.Contains("PED13-0012"))
            { //CAS: PED13-0012
                    display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.OneBeep, DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.Red, "-2");
                    display.SetDisplayPrint("002");
                    display.Message = new DPI1.Message(DPI1.Blinker.Normal, DPI1.BeepType.OneBeep, DPI1.ChannelNum.One, DPI1.KeySound.Disabled, DPI1.Color.Blue, "3");
                    display.SetDisplayPrint("003");
            }
        }
        private void checkKeyPress(KeyPress _RxKeyPress)
        {
            if (!ckbTestMode.Checked) return;
            ChannelNum kc = new ChannelNum();
            if (_RxKeyPress.Channel.ToString() == "1") kc = ChannelNum.One;
            else kc = ChannelNum.Two;
            if (_RxKeyPress.KeySymbol == "V")
                  display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.OneBeep, DPI1.ArrowsPos.None, kc, DPI1.KeySound.Disabled, DPI1.Color.None, _RxKeyPress.id);
            else if (_RxKeyPress.KeySymbol == "+")
                  display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.DoubleShortBeep, DPI1.ArrowsPos.RightUp, kc, DPI1.KeySound.Disabled, DPI1.Color.Blue, _RxKeyPress.KeySymbol.ToString());
            else if (_RxKeyPress.KeySymbol == "-")
                display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.DoubleLongBeep, DPI1.ArrowsPos.RightDown, kc, DPI1.KeySound.Disabled, DPI1.Color.Red, _RxKeyPress.KeySymbol.ToString());
            else if (_RxKeyPress.KeySymbol.ToUpper() == "F")
                display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.DoubleLongBeep, DPI1.ArrowsPos.LeftDown, kc, DPI1.KeySound.Disabled, DPI1.Color.Green, "8888XXXXTTTT*");
            else if (_RxKeyPress.KeySymbol.ToUpper() == "X")
                display.Message = new DPI1.Message(DPI1.Blinker.NoBlink, DPI1.BeepType.OneBeep, DPI1.ArrowsPos.Up, kc, DPI1.KeySound.Disabled, DPI1.Color.White, _RxKeyPress.KeySymbol.ToString());
            
            display.SetDisplayPrint(_RxKeyPress.id);

        }

        #endregion

        #region ADVANCED FUNCTIONS
        private void tSDownload_Click(object sender, EventArgs e)
        {
            saveFileDlg.Title = "Save DPI Configuration";
            saveFileDlg.DefaultExt = ".xml";
            saveFileDlg.Filter = "Config dpi|*.xml";
            if (saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                display.Save(saveFileDlg.FileName);
            }
        }

        

        private void tSCargar_Click(object sender, EventArgs e)
        {
            openFileDlg.Title = "Load DPI Configuration";
            openFileDlg.DefaultExt = ".xml";
            openFileDlg.Filter = "Config dpi|*.xml";
            if (openFileDlg.ShowDialog() == DialogResult.OK)
            {
                if (MessageBox.Show("IMPORTANT: This frame stores contend to microSD and to EEPROM node. The repeated use of this function can kill the stored content on microSD and cause malfunction of DPI interface."
                    + Environment.NewLine+"The current operation will override the DPI1 network configuration." 
                    + Environment.NewLine+ "Upload configuration?", "Attention", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) 
                    == DialogResult.OK)
                {
                    display.Load(openFileDlg.FileName);
                }
                
            }
        }

        private void bProgramNodeID_Click(object sender, EventArgs e)
        {
            int OldID = Convert.ToInt32(txtOldID.Text);
            int NewID = Convert.ToInt32(txtNewID.Text);
            display.ProgramNodeID((ChannelNum)cmboBoxChannel.SelectedItem, OldID.ToString("000"), NewID.ToString("000"));
        }

        private void bResetChannel_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Reset all the nodes of Channel " + cmboBoxChannel.Text + " ?", "Attention", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2)
                == DialogResult.OK)
            {
                display.ProgramNodeID((ChannelNum)cmboBoxChannel.SelectedItem,"252", "000");
                gBFirstTimeSetup.Enabled = true;
            }
            else gBFirstTimeSetup.Enabled = false;
        }
        private void bStart_Click(object sender, EventArgs e)
        {
            bFinish.Enabled = true;
            bStart.Enabled = false;
            int StartId = Convert.ToInt32(txtIDStart.Text);
            if (StartId > 0) display.ProgramIncInit((ChannelNum)cmboBoxChannel.SelectedItem, StartId.ToString("000"));
        }

        private void bFinish_Click(object sender, EventArgs e)
        {
            bStart.Enabled = true;
            bFinish.Enabled = false;
            display.ProgramIncEnd();
  
        }

 

        private void electrotecsc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.electrotecsc.com");
        }

       
        private void txtNewID_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNewID.Text)) bProgramNodeID.Enabled = true;
            else bProgramNodeID.Enabled = false;
        }
        #endregion

        private void uscNetDistribution2_Node_Clicked(object sender, EventArgs e)
        {
            fAction action=new fAction(display,((uscNetDistribution)sender).MSelectedNode,((uscNetDistribution)sender).MChannel);
            action.ShowDialog();
            if (action.DialogResult == DialogResult.OK) {  }


        }

        private void fMain_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            String openPDFFile = @".\doc.pdf";
            System.IO.File.WriteAllBytes(openPDFFile, TestDPI1.Properties.Resources.Software_TestDP1__MAN_0018_01_);
            System.Diagnostics.Process.Start(openPDFFile);
        }

  


      

       
   


































    }
}
