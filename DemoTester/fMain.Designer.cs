﻿namespace TestDPI1
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.TSTop = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsIP = new System.Windows.Forms.ToolStripTextBox();
            this.lPort = new System.Windows.Forms.ToolStripLabel();
            this.tsPort = new System.Windows.Forms.ToolStripTextBox();
            this.bConnect = new System.Windows.Forms.ToolStripButton();
            this.bDisconnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tSRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.TSBottom = new System.Windows.Forms.StatusStrip();
            this.tsTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSConnected = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSDisConnected = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSError = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pgGrid = new System.Windows.Forms.PropertyGrid();
            this.pTools = new System.Windows.Forms.Panel();
            this.gbMaintenance = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ckbTestMode = new System.Windows.Forms.CheckBox();
            this.gbTransmit = new System.Windows.Forms.GroupBox();
            this.txtSendAll = new System.Windows.Forms.TextBox();
            this.bSendAll = new System.Windows.Forms.Button();
            this.gbNode = new System.Windows.Forms.GroupBox();
            this.ChannelComboBox = new System.Windows.Forms.ComboBox();
            this.NodeComboBox = new System.Windows.Forms.ComboBox();
            this.cBoxBroadcast = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gbOther = new System.Windows.Forms.GroupBox();
            this.txtTotalNodes = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.bNetDistribution = new System.Windows.Forms.Button();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.bSerial = new System.Windows.Forms.Button();
            this.txtOpenSessions = new System.Windows.Forms.TextBox();
            this.bOpenSessions = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.electrotecsc = new System.Windows.Forms.LinkLabel();
            this.lblDLLVersion = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.timerCheckData = new System.Windows.Forms.Timer(this.components);
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tab1 = new System.Windows.Forms.TabPage();
            this.tabCom = new System.Windows.Forms.TabControl();
            this.tabEvents = new System.Windows.Forms.TabPage();
            this.spEvents = new System.Windows.Forms.SplitContainer();
            this.txtEvents = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bClrEvents = new System.Windows.Forms.Button();
            this.bClrRxBuffer = new System.Windows.Forms.Button();
            this.txtRxbuffer = new System.Windows.Forms.TextBox();
            this.LRxBuffer = new System.Windows.Forms.Label();
            this.tabRxKeypress = new System.Windows.Forms.TabPage();
            this.txtRxKeypressBuffer = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bClrKeypress = new System.Windows.Forms.Button();
            this.tabRxAlarms = new System.Windows.Forms.TabPage();
            this.txtRxAlarms = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.bClrAlarms = new System.Windows.Forms.Button();
            this.tabSerialBuffer = new System.Windows.Forms.TabPage();
            this.txtSerialBuffer = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.bClrSerialBuffer = new System.Windows.Forms.Button();
            this.tab0 = new System.Windows.Forms.TabPage();
            this.tab2 = new System.Windows.Forms.TabPage();
            this.tab3 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gBProgramNodes = new System.Windows.Forms.GroupBox();
            this.gBFirstTimeSetup = new System.Windows.Forms.GroupBox();
            this.bFinish = new System.Windows.Forms.Button();
            this.bStart = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtIDStart = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.gBSelectChannel = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmboBoxChannel = new System.Windows.Forms.ComboBox();
            this.bResetChannel = new System.Windows.Forms.Button();
            this.gBChangeNodeID = new System.Windows.Forms.GroupBox();
            this.txtOldID = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtNewID = new System.Windows.Forms.TextBox();
            this.bProgramNodeID = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.txtAdvice = new System.Windows.Forms.TextBox();
            this.tSAdv = new System.Windows.Forms.ToolStrip();
            this.tSSave = new System.Windows.Forms.ToolStripButton();
            this.tSload = new System.Windows.Forms.ToolStripButton();
            this.saveFileDlg = new System.Windows.Forms.SaveFileDialog();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.uscNetDistribution2 = new TestDPI1.uscNetDistribution();
            this.uscActions1 = new TestDPI1.uscActions();
            this.TSTop.SuspendLayout();
            this.TSBottom.SuspendLayout();
            this.pTools.SuspendLayout();
            this.gbMaintenance.SuspendLayout();
            this.gbTransmit.SuspendLayout();
            this.gbNode.SuspendLayout();
            this.gbOther.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tab1.SuspendLayout();
            this.tabCom.SuspendLayout();
            this.tabEvents.SuspendLayout();
            this.spEvents.Panel1.SuspendLayout();
            this.spEvents.Panel2.SuspendLayout();
            this.spEvents.SuspendLayout();
            this.tabRxKeypress.SuspendLayout();
            this.tabRxAlarms.SuspendLayout();
            this.tabSerialBuffer.SuspendLayout();
            this.tab0.SuspendLayout();
            this.tab2.SuspendLayout();
            this.tab3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.gBProgramNodes.SuspendLayout();
            this.gBFirstTimeSetup.SuspendLayout();
            this.gBSelectChannel.SuspendLayout();
            this.gBChangeNodeID.SuspendLayout();
            this.tSAdv.SuspendLayout();
            this.SuspendLayout();
            // 
            // TSTop
            // 
            this.TSTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tsIP,
            this.lPort,
            this.tsPort,
            this.bConnect,
            this.bDisconnect,
            this.toolStripSeparator1,
            this.tSRefresh,
            this.toolStripSeparator2});
            this.TSTop.Location = new System.Drawing.Point(0, 57);
            this.TSTop.Name = "TSTop";
            this.TSTop.Size = new System.Drawing.Size(1011, 25);
            this.TSTop.TabIndex = 0;
            this.TSTop.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(17, 22);
            this.toolStripLabel1.Text = "IP";
            // 
            // tsIP
            // 
            this.tsIP.MaxLength = 15;
            this.tsIP.Name = "tsIP";
            this.tsIP.Size = new System.Drawing.Size(80, 25);
            this.tsIP.Text = "192.168.0.200";
            // 
            // lPort
            // 
            this.lPort.Name = "lPort";
            this.lPort.Size = new System.Drawing.Size(27, 22);
            this.lPort.Text = "Port";
            // 
            // tsPort
            // 
            this.tsPort.MaxLength = 5;
            this.tsPort.Name = "tsPort";
            this.tsPort.Size = new System.Drawing.Size(50, 25);
            this.tsPort.Text = "16";
            // 
            // bConnect
            // 
            this.bConnect.BackColor = System.Drawing.SystemColors.Control;
            this.bConnect.Image = global::TestDPI1.Properties.Resources.play;
            this.bConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(67, 22);
            this.bConnect.Text = "Connect";
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // bDisconnect
            // 
            this.bDisconnect.Image = global::TestDPI1.Properties.Resources.stop;
            this.bDisconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bDisconnect.Name = "bDisconnect";
            this.bDisconnect.Size = new System.Drawing.Size(79, 22);
            this.bDisconnect.Text = "Disconnect";
            this.bDisconnect.Click += new System.EventHandler(this.bDisconnect_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tSRefresh
            // 
            this.tSRefresh.Image = global::TestDPI1.Properties.Resources.refresh;
            this.tSRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSRefresh.Name = "tSRefresh";
            this.tSRefresh.Size = new System.Drawing.Size(65, 22);
            this.tSRefresh.Text = "Refresh";
            this.tSRefresh.Click += new System.EventHandler(this.bNetDistribution_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // TSBottom
            // 
            this.TSBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTime,
            this.tSConnected,
            this.tSDisConnected,
            this.TSError,
            this.tSStatus});
            this.TSBottom.Location = new System.Drawing.Point(0, 522);
            this.TSBottom.Name = "TSBottom";
            this.TSBottom.Size = new System.Drawing.Size(1011, 22);
            this.TSBottom.TabIndex = 1;
            this.TSBottom.Text = "statusStrip1";
            // 
            // tsTime
            // 
            this.tsTime.Name = "tsTime";
            this.tsTime.Size = new System.Drawing.Size(69, 17);
            this.tsTime.Text = "hh:mm:ss.ttt";
            // 
            // tSConnected
            // 
            this.tSConnected.Image = global::TestDPI1.Properties.Resources.green_button;
            this.tSConnected.Name = "tSConnected";
            this.tSConnected.Size = new System.Drawing.Size(75, 17);
            this.tSConnected.Text = "Connected";
            // 
            // tSDisConnected
            // 
            this.tSDisConnected.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tSDisConnected.Image = global::TestDPI1.Properties.Resources.red_button;
            this.tSDisConnected.Name = "tSDisConnected";
            this.tSDisConnected.Size = new System.Drawing.Size(87, 17);
            this.tSDisConnected.Text = "Disconnected";
            // 
            // TSError
            // 
            this.TSError.Image = global::TestDPI1.Properties.Resources.warning;
            this.TSError.Name = "TSError";
            this.TSError.Size = new System.Drawing.Size(63, 17);
            this.TSError.Text = "Warning";
            this.TSError.Visible = false;
            // 
            // tSStatus
            // 
            this.tSStatus.Name = "tSStatus";
            this.tSStatus.Size = new System.Drawing.Size(38, 17);
            this.tSStatus.Text = "Status";
            // 
            // pgGrid
            // 
            this.pgGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgGrid.Location = new System.Drawing.Point(0, 25);
            this.pgGrid.Margin = new System.Windows.Forms.Padding(5);
            this.pgGrid.Name = "pgGrid";
            this.pgGrid.Size = new System.Drawing.Size(598, 380);
            this.pgGrid.TabIndex = 8;
            // 
            // pTools
            // 
            this.pTools.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pTools.Controls.Add(this.uscActions1);
            this.pTools.Controls.Add(this.gbMaintenance);
            this.pTools.Controls.Add(this.gbTransmit);
            this.pTools.Controls.Add(this.gbNode);
            this.pTools.Controls.Add(this.gbOther);
            this.pTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pTools.Location = new System.Drawing.Point(0, 0);
            this.pTools.Name = "pTools";
            this.pTools.Size = new System.Drawing.Size(1003, 411);
            this.pTools.TabIndex = 4;
            // 
            // gbMaintenance
            // 
            this.gbMaintenance.BackgroundImage = global::TestDPI1.Properties.Resources.tools;
            this.gbMaintenance.Controls.Add(this.textBox2);
            this.gbMaintenance.Controls.Add(this.ckbTestMode);
            this.gbMaintenance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMaintenance.Location = new System.Drawing.Point(6, 117);
            this.gbMaintenance.Name = "gbMaintenance";
            this.gbMaintenance.Size = new System.Drawing.Size(288, 136);
            this.gbMaintenance.TabIndex = 71;
            this.gbMaintenance.TabStop = false;
            this.gbMaintenance.Text = "Maintenance";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Control;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBox2.Location = new System.Drawing.Point(23, 73);
            this.textBox2.MaxLength = 5000;
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(256, 43);
            this.textBox2.TabIndex = 67;
            this.textBox2.Text = "- Show the ID of each Node\r\n- Press the node keys to validate";
            // 
            // ckbTestMode
            // 
            this.ckbTestMode.BackColor = System.Drawing.Color.Orange;
            this.ckbTestMode.Image = global::TestDPI1.Properties.Resources.accept;
            this.ckbTestMode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ckbTestMode.Location = new System.Drawing.Point(23, 35);
            this.ckbTestMode.Name = "ckbTestMode";
            this.ckbTestMode.Size = new System.Drawing.Size(256, 32);
            this.ckbTestMode.TabIndex = 64;
            this.ckbTestMode.Text = "Test Mode";
            this.ckbTestMode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ckbTestMode.UseVisualStyleBackColor = false;
            this.ckbTestMode.CheckedChanged += new System.EventHandler(this.ckbTestMode_CheckedChanged);
            // 
            // gbTransmit
            // 
            this.gbTransmit.BackColor = System.Drawing.SystemColors.Control;
            this.gbTransmit.Controls.Add(this.txtSendAll);
            this.gbTransmit.Controls.Add(this.bSendAll);
            this.gbTransmit.Location = new System.Drawing.Point(595, 136);
            this.gbTransmit.Name = "gbTransmit";
            this.gbTransmit.Size = new System.Drawing.Size(285, 96);
            this.gbTransmit.TabIndex = 69;
            this.gbTransmit.TabStop = false;
            this.gbTransmit.Text = "Other Transmit Options (Programming Tools)";
            // 
            // txtSendAll
            // 
            this.txtSendAll.BackColor = System.Drawing.SystemColors.Control;
            this.txtSendAll.Location = new System.Drawing.Point(6, 19);
            this.txtSendAll.MaxLength = 5000;
            this.txtSendAll.Multiline = true;
            this.txtSendAll.Name = "txtSendAll";
            this.txtSendAll.ReadOnly = true;
            this.txtSendAll.Size = new System.Drawing.Size(142, 66);
            this.txtSendAll.TabIndex = 66;
            this.txtSendAll.Text = "- Send a message packet to 1 - 25 nodes\r\n- Receives the acknowlegde response from" +
                " each node";
            // 
            // bSendAll
            // 
            this.bSendAll.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bSendAll.Image = global::TestDPI1.Properties.Resources.page_down;
            this.bSendAll.Location = new System.Drawing.Point(154, 19);
            this.bSendAll.Name = "bSendAll";
            this.bSendAll.Size = new System.Drawing.Size(119, 43);
            this.bSendAll.TabIndex = 62;
            this.bSendAll.Text = "Send to nodes with ACK response";
            this.bSendAll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bSendAll.UseVisualStyleBackColor = false;
            this.bSendAll.Click += new System.EventHandler(this.bSendAll_Click);
            // 
            // gbNode
            // 
            this.gbNode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbNode.Controls.Add(this.ChannelComboBox);
            this.gbNode.Controls.Add(this.NodeComboBox);
            this.gbNode.Controls.Add(this.cBoxBroadcast);
            this.gbNode.Controls.Add(this.label3);
            this.gbNode.Controls.Add(this.label16);
            this.gbNode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbNode.Location = new System.Drawing.Point(4, 4);
            this.gbNode.Name = "gbNode";
            this.gbNode.Size = new System.Drawing.Size(288, 107);
            this.gbNode.TabIndex = 68;
            this.gbNode.TabStop = false;
            this.gbNode.Text = "Select a Node";
            // 
            // ChannelComboBox
            // 
            this.ChannelComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChannelComboBox.FormattingEnabled = true;
            this.ChannelComboBox.Location = new System.Drawing.Point(56, 19);
            this.ChannelComboBox.Name = "ChannelComboBox";
            this.ChannelComboBox.Size = new System.Drawing.Size(216, 21);
            this.ChannelComboBox.TabIndex = 50;
            this.ChannelComboBox.SelectedIndexChanged += new System.EventHandler(this.ChannelComboBox_SelectedIndexChanged);
            // 
            // NodeComboBox
            // 
            this.NodeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NodeComboBox.FormattingEnabled = true;
            this.NodeComboBox.Location = new System.Drawing.Point(56, 46);
            this.NodeComboBox.Name = "NodeComboBox";
            this.NodeComboBox.Size = new System.Drawing.Size(216, 21);
            this.NodeComboBox.TabIndex = 48;
            this.NodeComboBox.SelectedIndexChanged += new System.EventHandler(this.NodeComboBox_SelectedIndexChanged);
            // 
            // cBoxBroadcast
            // 
            this.cBoxBroadcast.AutoSize = true;
            this.cBoxBroadcast.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cBoxBroadcast.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxBroadcast.Location = new System.Drawing.Point(46, 73);
            this.cBoxBroadcast.Name = "cBoxBroadcast";
            this.cBoxBroadcast.Size = new System.Drawing.Size(225, 18);
            this.cBoxBroadcast.TabIndex = 49;
            this.cBoxBroadcast.Text = "All Nodes in the channel (Broadcast)";
            this.cBoxBroadcast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cBoxBroadcast.UseVisualStyleBackColor = true;
            this.cBoxBroadcast.CheckedChanged += new System.EventHandler(this.NodeComboBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-1, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 51;
            this.label3.Text = "Channel";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 16);
            this.label16.TabIndex = 52;
            this.label16.Text = "Node";
            // 
            // gbOther
            // 
            this.gbOther.Controls.Add(this.txtTotalNodes);
            this.gbOther.Controls.Add(this.textBox4);
            this.gbOther.Controls.Add(this.textBox3);
            this.gbOther.Controls.Add(this.bNetDistribution);
            this.gbOther.Controls.Add(this.txtVersion);
            this.gbOther.Controls.Add(this.bSerial);
            this.gbOther.Controls.Add(this.txtOpenSessions);
            this.gbOther.Controls.Add(this.bOpenSessions);
            this.gbOther.Location = new System.Drawing.Point(592, 8);
            this.gbOther.Name = "gbOther";
            this.gbOther.Size = new System.Drawing.Size(288, 120);
            this.gbOther.TabIndex = 66;
            this.gbOther.TabStop = false;
            this.gbOther.Text = "Other Actions";
            // 
            // txtTotalNodes
            // 
            this.txtTotalNodes.Location = new System.Drawing.Point(237, 22);
            this.txtTotalNodes.MaxLength = 2;
            this.txtTotalNodes.Name = "txtTotalNodes";
            this.txtTotalNodes.ReadOnly = true;
            this.txtTotalNodes.Size = new System.Drawing.Size(39, 20);
            this.txtTotalNodes.TabIndex = 69;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Control;
            this.textBox4.Location = new System.Drawing.Point(8, 84);
            this.textBox4.MaxLength = 5000;
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(106, 23);
            this.textBox4.TabIndex = 68;
            this.textBox4.Text = " Firmware Version";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Control;
            this.textBox3.Location = new System.Drawing.Point(7, 15);
            this.textBox3.MaxLength = 5000;
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(107, 32);
            this.textBox3.TabIndex = 67;
            this.textBox3.Text = " Obtains the list of detected Nodes";
            // 
            // bNetDistribution
            // 
            this.bNetDistribution.Image = global::TestDPI1.Properties.Resources.refresh_page;
            this.bNetDistribution.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bNetDistribution.Location = new System.Drawing.Point(121, 13);
            this.bNetDistribution.Name = "bNetDistribution";
            this.bNetDistribution.Size = new System.Drawing.Size(107, 37);
            this.bNetDistribution.TabIndex = 19;
            this.bNetDistribution.Text = "Get Network Distribution";
            this.bNetDistribution.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bNetDistribution.UseVisualStyleBackColor = true;
            this.bNetDistribution.Click += new System.EventHandler(this.bNetDistribution_Click);
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(237, 86);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.ReadOnly = true;
            this.txtVersion.Size = new System.Drawing.Size(37, 20);
            this.txtVersion.TabIndex = 65;
            // 
            // bSerial
            // 
            this.bSerial.Location = new System.Drawing.Point(120, 84);
            this.bSerial.Name = "bSerial";
            this.bSerial.Size = new System.Drawing.Size(109, 23);
            this.bSerial.TabIndex = 20;
            this.bSerial.Text = "Get Serial";
            this.bSerial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bSerial.UseVisualStyleBackColor = true;
            this.bSerial.Click += new System.EventHandler(this.bSerial_Click);
            // 
            // txtOpenSessions
            // 
            this.txtOpenSessions.Location = new System.Drawing.Point(237, 58);
            this.txtOpenSessions.MaxLength = 2;
            this.txtOpenSessions.Name = "txtOpenSessions";
            this.txtOpenSessions.ReadOnly = true;
            this.txtOpenSessions.Size = new System.Drawing.Size(37, 20);
            this.txtOpenSessions.TabIndex = 64;
            // 
            // bOpenSessions
            // 
            this.bOpenSessions.Location = new System.Drawing.Point(120, 58);
            this.bOpenSessions.Name = "bOpenSessions";
            this.bOpenSessions.Size = new System.Drawing.Size(108, 23);
            this.bOpenSessions.TabIndex = 21;
            this.bOpenSessions.Text = "Get Open sessions";
            this.bOpenSessions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOpenSessions.UseVisualStyleBackColor = true;
            this.bOpenSessions.Click += new System.EventHandler(this.bOpenSessions_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.electrotecsc);
            this.panel1.Controls.Add(this.lblDLLVersion);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1011, 57);
            this.panel1.TabIndex = 5;
            // 
            // electrotecsc
            // 
            this.electrotecsc.AutoSize = true;
            this.electrotecsc.Location = new System.Drawing.Point(756, 19);
            this.electrotecsc.Name = "electrotecsc";
            this.electrotecsc.Size = new System.Drawing.Size(115, 13);
            this.electrotecsc.TabIndex = 25;
            this.electrotecsc.TabStop = true;
            this.electrotecsc.Text = "www.electrotecsc.com";
            this.electrotecsc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.electrotecsc_LinkClicked);
            // 
            // lblDLLVersion
            // 
            this.lblDLLVersion.AutoSize = true;
            this.lblDLLVersion.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDLLVersion.Location = new System.Drawing.Point(124, 32);
            this.lblDLLVersion.Name = "lblDLLVersion";
            this.lblDLLVersion.Size = new System.Drawing.Size(111, 14);
            this.lblDLLVersion.TabIndex = 24;
            this.lblDLLVersion.Text = "APP dll Test v1.8";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(121, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 23);
            this.label4.TabIndex = 23;
            this.label4.Text = "DPI - Test tools";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.LightGray;
            this.pictureBox2.Image = global::TestDPI1.Properties.Resources.ELECTRO_logocolorsWEB;
            this.pictureBox2.InitialImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(113, 43);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            // 
            // timerCheckData
            // 
            this.timerCheckData.Tick += new System.EventHandler(this.timerCheckData_Tick);
            // 
            // tabMain
            // 
            this.tabMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabMain.Controls.Add(this.tab1);
            this.tabMain.Controls.Add(this.tab0);
            this.tabMain.Controls.Add(this.tab2);
            this.tabMain.Controls.Add(this.tab3);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 82);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1011, 440);
            this.tabMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabMain.TabIndex = 6;
            // 
            // tab1
            // 
            this.tab1.Controls.Add(this.tabCom);
            this.tab1.Location = new System.Drawing.Point(4, 25);
            this.tab1.Name = "tab1";
            this.tab1.Size = new System.Drawing.Size(1003, 411);
            this.tab1.TabIndex = 1;
            this.tab1.Text = "Communications";
            this.tab1.UseVisualStyleBackColor = true;
            // 
            // tabCom
            // 
            this.tabCom.Controls.Add(this.tabEvents);
            this.tabCom.Controls.Add(this.tabRxKeypress);
            this.tabCom.Controls.Add(this.tabRxAlarms);
            this.tabCom.Controls.Add(this.tabSerialBuffer);
            this.tabCom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCom.Location = new System.Drawing.Point(0, 0);
            this.tabCom.Name = "tabCom";
            this.tabCom.SelectedIndex = 0;
            this.tabCom.Size = new System.Drawing.Size(1003, 411);
            this.tabCom.TabIndex = 21;
            // 
            // tabEvents
            // 
            this.tabEvents.Controls.Add(this.spEvents);
            this.tabEvents.Location = new System.Drawing.Point(4, 22);
            this.tabEvents.Name = "tabEvents";
            this.tabEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabEvents.Size = new System.Drawing.Size(995, 385);
            this.tabEvents.TabIndex = 4;
            this.tabEvents.Text = "Events";
            this.tabEvents.UseVisualStyleBackColor = true;
            // 
            // spEvents
            // 
            this.spEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spEvents.Location = new System.Drawing.Point(3, 3);
            this.spEvents.Name = "spEvents";
            // 
            // spEvents.Panel1
            // 
            this.spEvents.Panel1.Controls.Add(this.txtEvents);
            this.spEvents.Panel1.Controls.Add(this.label1);
            this.spEvents.Panel1.Controls.Add(this.bClrEvents);
            // 
            // spEvents.Panel2
            // 
            this.spEvents.Panel2.Controls.Add(this.bClrRxBuffer);
            this.spEvents.Panel2.Controls.Add(this.txtRxbuffer);
            this.spEvents.Panel2.Controls.Add(this.LRxBuffer);
            this.spEvents.Size = new System.Drawing.Size(989, 379);
            this.spEvents.SplitterDistance = 326;
            this.spEvents.TabIndex = 32;
            // 
            // txtEvents
            // 
            this.txtEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEvents.Location = new System.Drawing.Point(0, 16);
            this.txtEvents.Multiline = true;
            this.txtEvents.Name = "txtEvents";
            this.txtEvents.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEvents.Size = new System.Drawing.Size(326, 363);
            this.txtEvents.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 16);
            this.label1.TabIndex = 30;
            this.label1.Text = "Events ";
            // 
            // bClrEvents
            // 
            this.bClrEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClrEvents.Location = new System.Drawing.Point(262, -3);
            this.bClrEvents.Name = "bClrEvents";
            this.bClrEvents.Size = new System.Drawing.Size(61, 19);
            this.bClrEvents.TabIndex = 31;
            this.bClrEvents.Text = "Clear";
            this.bClrEvents.UseVisualStyleBackColor = true;
            this.bClrEvents.Click += new System.EventHandler(this.bClr_Click);
            // 
            // bClrRxBuffer
            // 
            this.bClrRxBuffer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClrRxBuffer.Location = new System.Drawing.Point(595, -1);
            this.bClrRxBuffer.Name = "bClrRxBuffer";
            this.bClrRxBuffer.Size = new System.Drawing.Size(61, 19);
            this.bClrRxBuffer.TabIndex = 20;
            this.bClrRxBuffer.Text = "Clear";
            this.bClrRxBuffer.UseVisualStyleBackColor = true;
            this.bClrRxBuffer.Click += new System.EventHandler(this.bClr_Click);
            // 
            // txtRxbuffer
            // 
            this.txtRxbuffer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRxbuffer.Location = new System.Drawing.Point(0, 16);
            this.txtRxbuffer.Multiline = true;
            this.txtRxbuffer.Name = "txtRxbuffer";
            this.txtRxbuffer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRxbuffer.Size = new System.Drawing.Size(659, 363);
            this.txtRxbuffer.TabIndex = 6;
            // 
            // LRxBuffer
            // 
            this.LRxBuffer.AutoSize = true;
            this.LRxBuffer.Dock = System.Windows.Forms.DockStyle.Top;
            this.LRxBuffer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LRxBuffer.Location = new System.Drawing.Point(0, 0);
            this.LRxBuffer.Name = "LRxBuffer";
            this.LRxBuffer.Size = new System.Drawing.Size(147, 16);
            this.LRxBuffer.TabIndex = 5;
            this.LRxBuffer.Text = "Data received - RxBuffer";
            // 
            // tabRxKeypress
            // 
            this.tabRxKeypress.Controls.Add(this.txtRxKeypressBuffer);
            this.tabRxKeypress.Controls.Add(this.label9);
            this.tabRxKeypress.Controls.Add(this.bClrKeypress);
            this.tabRxKeypress.Location = new System.Drawing.Point(4, 22);
            this.tabRxKeypress.Name = "tabRxKeypress";
            this.tabRxKeypress.Padding = new System.Windows.Forms.Padding(3);
            this.tabRxKeypress.Size = new System.Drawing.Size(995, 385);
            this.tabRxKeypress.TabIndex = 1;
            this.tabRxKeypress.Text = "RxKeyPress";
            this.tabRxKeypress.UseVisualStyleBackColor = true;
            // 
            // txtRxKeypressBuffer
            // 
            this.txtRxKeypressBuffer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRxKeypressBuffer.Location = new System.Drawing.Point(3, 19);
            this.txtRxKeypressBuffer.Multiline = true;
            this.txtRxKeypressBuffer.Name = "txtRxKeypressBuffer";
            this.txtRxKeypressBuffer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRxKeypressBuffer.Size = new System.Drawing.Size(989, 363);
            this.txtRxKeypressBuffer.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(242, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "KeyPressed reception - RxKeypressBuffer";
            // 
            // bClrKeypress
            // 
            this.bClrKeypress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClrKeypress.Location = new System.Drawing.Point(928, 2);
            this.bClrKeypress.Name = "bClrKeypress";
            this.bClrKeypress.Size = new System.Drawing.Size(61, 19);
            this.bClrKeypress.TabIndex = 22;
            this.bClrKeypress.Text = "Clear";
            this.bClrKeypress.UseVisualStyleBackColor = true;
            this.bClrKeypress.Click += new System.EventHandler(this.bClr_Click);
            // 
            // tabRxAlarms
            // 
            this.tabRxAlarms.Controls.Add(this.txtRxAlarms);
            this.tabRxAlarms.Controls.Add(this.label10);
            this.tabRxAlarms.Controls.Add(this.bClrAlarms);
            this.tabRxAlarms.Location = new System.Drawing.Point(4, 22);
            this.tabRxAlarms.Name = "tabRxAlarms";
            this.tabRxAlarms.Padding = new System.Windows.Forms.Padding(3);
            this.tabRxAlarms.Size = new System.Drawing.Size(995, 385);
            this.tabRxAlarms.TabIndex = 2;
            this.tabRxAlarms.Text = "RxAlarms";
            this.tabRxAlarms.UseVisualStyleBackColor = true;
            // 
            // txtRxAlarms
            // 
            this.txtRxAlarms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRxAlarms.Location = new System.Drawing.Point(3, 19);
            this.txtRxAlarms.Multiline = true;
            this.txtRxAlarms.Name = "txtRxAlarms";
            this.txtRxAlarms.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRxAlarms.Size = new System.Drawing.Size(989, 363);
            this.txtRxAlarms.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 16);
            this.label10.TabIndex = 24;
            this.label10.Text = "Alarms reception - RxAlarmBuffer";
            // 
            // bClrAlarms
            // 
            this.bClrAlarms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClrAlarms.Location = new System.Drawing.Point(928, 2);
            this.bClrAlarms.Name = "bClrAlarms";
            this.bClrAlarms.Size = new System.Drawing.Size(61, 19);
            this.bClrAlarms.TabIndex = 25;
            this.bClrAlarms.Text = "Clear";
            this.bClrAlarms.UseVisualStyleBackColor = true;
            this.bClrAlarms.Click += new System.EventHandler(this.bClr_Click);
            // 
            // tabSerialBuffer
            // 
            this.tabSerialBuffer.Controls.Add(this.txtSerialBuffer);
            this.tabSerialBuffer.Controls.Add(this.label17);
            this.tabSerialBuffer.Controls.Add(this.bClrSerialBuffer);
            this.tabSerialBuffer.Location = new System.Drawing.Point(4, 22);
            this.tabSerialBuffer.Name = "tabSerialBuffer";
            this.tabSerialBuffer.Padding = new System.Windows.Forms.Padding(3);
            this.tabSerialBuffer.Size = new System.Drawing.Size(995, 385);
            this.tabSerialBuffer.TabIndex = 3;
            this.tabSerialBuffer.Text = "SerialBuffer";
            this.tabSerialBuffer.UseVisualStyleBackColor = true;
            // 
            // txtSerialBuffer
            // 
            this.txtSerialBuffer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSerialBuffer.Location = new System.Drawing.Point(3, 19);
            this.txtSerialBuffer.Multiline = true;
            this.txtSerialBuffer.Name = "txtSerialBuffer";
            this.txtSerialBuffer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSerialBuffer.Size = new System.Drawing.Size(989, 363);
            this.txtSerialBuffer.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(146, 16);
            this.label17.TabIndex = 27;
            this.label17.Text = "Serial - RS232 Interface";
            // 
            // bClrSerialBuffer
            // 
            this.bClrSerialBuffer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClrSerialBuffer.Location = new System.Drawing.Point(928, 2);
            this.bClrSerialBuffer.Name = "bClrSerialBuffer";
            this.bClrSerialBuffer.Size = new System.Drawing.Size(61, 19);
            this.bClrSerialBuffer.TabIndex = 28;
            this.bClrSerialBuffer.Text = "Clear";
            this.bClrSerialBuffer.UseVisualStyleBackColor = true;
            this.bClrSerialBuffer.Click += new System.EventHandler(this.bClr_Click);
            // 
            // tab0
            // 
            this.tab0.Controls.Add(this.uscNetDistribution2);
            this.tab0.Location = new System.Drawing.Point(4, 25);
            this.tab0.Name = "tab0";
            this.tab0.Padding = new System.Windows.Forms.Padding(3);
            this.tab0.Size = new System.Drawing.Size(1003, 411);
            this.tab0.TabIndex = 3;
            this.tab0.Text = "DP Nodes";
            this.tab0.UseVisualStyleBackColor = true;
            // 
            // tab2
            // 
            this.tab2.Controls.Add(this.pTools);
            this.tab2.Location = new System.Drawing.Point(4, 25);
            this.tab2.Name = "tab2";
            this.tab2.Size = new System.Drawing.Size(1003, 411);
            this.tab2.TabIndex = 2;
            this.tab2.Text = "Actions";
            this.tab2.UseVisualStyleBackColor = true;
            // 
            // tab3
            // 
            this.tab3.Controls.Add(this.panel9);
            this.tab3.Location = new System.Drawing.Point(4, 25);
            this.tab3.Name = "tab3";
            this.tab3.Padding = new System.Windows.Forms.Padding(3);
            this.tab3.Size = new System.Drawing.Size(1003, 411);
            this.tab3.TabIndex = 3;
            this.tab3.Text = "Advanced";
            this.tab3.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pgGrid);
            this.panel9.Controls.Add(this.gBProgramNodes);
            this.panel9.Controls.Add(this.tSAdv);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(997, 405);
            this.panel9.TabIndex = 11;
            // 
            // gBProgramNodes
            // 
            this.gBProgramNodes.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.gBProgramNodes.Controls.Add(this.gBFirstTimeSetup);
            this.gBProgramNodes.Controls.Add(this.gBSelectChannel);
            this.gBProgramNodes.Controls.Add(this.gBChangeNodeID);
            this.gBProgramNodes.Controls.Add(this.txtAdvice);
            this.gBProgramNodes.Dock = System.Windows.Forms.DockStyle.Right;
            this.gBProgramNodes.Location = new System.Drawing.Point(598, 25);
            this.gBProgramNodes.Name = "gBProgramNodes";
            this.gBProgramNodes.Size = new System.Drawing.Size(399, 380);
            this.gBProgramNodes.TabIndex = 14;
            this.gBProgramNodes.TabStop = false;
            this.gBProgramNodes.Text = "Programming Tools";
            // 
            // gBFirstTimeSetup
            // 
            this.gBFirstTimeSetup.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gBFirstTimeSetup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gBFirstTimeSetup.Controls.Add(this.bFinish);
            this.gBFirstTimeSetup.Controls.Add(this.bStart);
            this.gBFirstTimeSetup.Controls.Add(this.label19);
            this.gBFirstTimeSetup.Controls.Add(this.txtIDStart);
            this.gBFirstTimeSetup.Controls.Add(this.textBox1);
            this.gBFirstTimeSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBFirstTimeSetup.Enabled = false;
            this.gBFirstTimeSetup.Location = new System.Drawing.Point(3, 112);
            this.gBFirstTimeSetup.Name = "gBFirstTimeSetup";
            this.gBFirstTimeSetup.Size = new System.Drawing.Size(393, 215);
            this.gBFirstTimeSetup.TabIndex = 12;
            this.gBFirstTimeSetup.TabStop = false;
            this.gBFirstTimeSetup.Text = "First Time Set Up";
            // 
            // bFinish
            // 
            this.bFinish.BackColor = System.Drawing.Color.DarkOrange;
            this.bFinish.Enabled = false;
            this.bFinish.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bFinish.Location = new System.Drawing.Point(251, 186);
            this.bFinish.Name = "bFinish";
            this.bFinish.Size = new System.Drawing.Size(73, 23);
            this.bFinish.TabIndex = 55;
            this.bFinish.Text = "Finish";
            this.bFinish.UseVisualStyleBackColor = false;
            this.bFinish.Click += new System.EventHandler(this.bFinish_Click);
            // 
            // bStart
            // 
            this.bStart.BackColor = System.Drawing.SystemColors.Control;
            this.bStart.Location = new System.Drawing.Point(172, 186);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(73, 23);
            this.bStart.TabIndex = 54;
            this.bStart.Text = "Start";
            this.bStart.UseVisualStyleBackColor = false;
            this.bStart.Click += new System.EventHandler(this.bStart_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label19.Location = new System.Drawing.Point(47, 189);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 16);
            this.label19.TabIndex = 2;
            this.label19.Text = "ID to start";
            // 
            // txtIDStart
            // 
            this.txtIDStart.Location = new System.Drawing.Point(118, 188);
            this.txtIDStart.MaxLength = 3;
            this.txtIDStart.Name = "txtIDStart";
            this.txtIDStart.Size = new System.Drawing.Size(30, 20);
            this.txtIDStart.TabIndex = 1;
            this.txtIDStart.Text = "1";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox1.Location = new System.Drawing.Point(6, 19);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(433, 163);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // gBSelectChannel
            // 
            this.gBSelectChannel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gBSelectChannel.Controls.Add(this.label20);
            this.gBSelectChannel.Controls.Add(this.cmboBoxChannel);
            this.gBSelectChannel.Controls.Add(this.bResetChannel);
            this.gBSelectChannel.Dock = System.Windows.Forms.DockStyle.Top;
            this.gBSelectChannel.Location = new System.Drawing.Point(3, 61);
            this.gBSelectChannel.Name = "gBSelectChannel";
            this.gBSelectChannel.Size = new System.Drawing.Size(393, 51);
            this.gBSelectChannel.TabIndex = 54;
            this.gBSelectChannel.TabStop = false;
            this.gBSelectChannel.Text = "Select a Channel";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(47, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 16);
            this.label20.TabIndex = 53;
            this.label20.Text = "Channel";
            // 
            // cmboBoxChannel
            // 
            this.cmboBoxChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmboBoxChannel.FormattingEnabled = true;
            this.cmboBoxChannel.Location = new System.Drawing.Point(131, 20);
            this.cmboBoxChannel.Name = "cmboBoxChannel";
            this.cmboBoxChannel.Size = new System.Drawing.Size(114, 21);
            this.cmboBoxChannel.TabIndex = 52;
            this.cmboBoxChannel.SelectedIndexChanged += new System.EventHandler(this.ChannelComboBox_SelectedIndexChanged);
            // 
            // bResetChannel
            // 
            this.bResetChannel.BackColor = System.Drawing.SystemColors.Control;
            this.bResetChannel.Image = global::TestDPI1.Properties.Resources.security;
            this.bResetChannel.Location = new System.Drawing.Point(251, 20);
            this.bResetChannel.Name = "bResetChannel";
            this.bResetChannel.Size = new System.Drawing.Size(73, 21);
            this.bResetChannel.TabIndex = 4;
            this.bResetChannel.Text = "Reset";
            this.bResetChannel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bResetChannel.UseVisualStyleBackColor = false;
            this.bResetChannel.Click += new System.EventHandler(this.bResetChannel_Click);
            // 
            // gBChangeNodeID
            // 
            this.gBChangeNodeID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gBChangeNodeID.Controls.Add(this.txtOldID);
            this.gBChangeNodeID.Controls.Add(this.label27);
            this.gBChangeNodeID.Controls.Add(this.txtNewID);
            this.gBChangeNodeID.Controls.Add(this.bProgramNodeID);
            this.gBChangeNodeID.Controls.Add(this.label28);
            this.gBChangeNodeID.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gBChangeNodeID.Enabled = false;
            this.gBChangeNodeID.Location = new System.Drawing.Point(3, 327);
            this.gBChangeNodeID.Name = "gBChangeNodeID";
            this.gBChangeNodeID.Size = new System.Drawing.Size(393, 50);
            this.gBChangeNodeID.TabIndex = 13;
            this.gBChangeNodeID.TabStop = false;
            this.gBChangeNodeID.Text = "Change Node ID";
            // 
            // txtOldID
            // 
            this.txtOldID.Location = new System.Drawing.Point(118, 17);
            this.txtOldID.MaxLength = 3;
            this.txtOldID.Name = "txtOldID";
            this.txtOldID.Size = new System.Drawing.Size(30, 20);
            this.txtOldID.TabIndex = 61;
            this.txtOldID.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label27.Location = new System.Drawing.Point(160, 19);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 16);
            this.label27.TabIndex = 60;
            this.label27.Text = "New ID";
            // 
            // txtNewID
            // 
            this.txtNewID.Location = new System.Drawing.Point(215, 16);
            this.txtNewID.MaxLength = 3;
            this.txtNewID.Name = "txtNewID";
            this.txtNewID.Size = new System.Drawing.Size(30, 20);
            this.txtNewID.TabIndex = 59;
            this.txtNewID.Text = "1";
            this.txtNewID.TextChanged += new System.EventHandler(this.txtNewID_TextChanged);
            // 
            // bProgramNodeID
            // 
            this.bProgramNodeID.BackColor = System.Drawing.SystemColors.Control;
            this.bProgramNodeID.Location = new System.Drawing.Point(251, 17);
            this.bProgramNodeID.Name = "bProgramNodeID";
            this.bProgramNodeID.Size = new System.Drawing.Size(73, 20);
            this.bProgramNodeID.TabIndex = 58;
            this.bProgramNodeID.Text = "Change";
            this.bProgramNodeID.UseVisualStyleBackColor = false;
            this.bProgramNodeID.Click += new System.EventHandler(this.bProgramNodeID_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(69, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 16);
            this.label28.TabIndex = 57;
            this.label28.Text = "Old ID";
            // 
            // txtAdvice
            // 
            this.txtAdvice.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtAdvice.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtAdvice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdvice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtAdvice.Location = new System.Drawing.Point(3, 16);
            this.txtAdvice.Margin = new System.Windows.Forms.Padding(5);
            this.txtAdvice.Multiline = true;
            this.txtAdvice.Name = "txtAdvice";
            this.txtAdvice.ReadOnly = true;
            this.txtAdvice.Size = new System.Drawing.Size(393, 45);
            this.txtAdvice.TabIndex = 55;
            this.txtAdvice.Text = "IMPORTANT: This frame stores contend to microSD and to EEPROM node. The repeated " +
                "use of this function can kill the stored content on microSD and cause malfunctio" +
                "n of DPI interface.";
            // 
            // tSAdv
            // 
            this.tSAdv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSSave,
            this.tSload});
            this.tSAdv.Location = new System.Drawing.Point(0, 0);
            this.tSAdv.Name = "tSAdv";
            this.tSAdv.Size = new System.Drawing.Size(997, 25);
            this.tSAdv.TabIndex = 11;
            this.tSAdv.Text = "toolStrip1";
            // 
            // tSSave
            // 
            this.tSSave.Image = global::TestDPI1.Properties.Resources.blue_arrow_down;
            this.tSSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSSave.Name = "tSSave";
            this.tSSave.Size = new System.Drawing.Size(111, 22);
            this.tSSave.Text = "Download && Save";
            this.tSSave.Click += new System.EventHandler(this.tSDownload_Click);
            // 
            // tSload
            // 
            this.tSload.Image = global::TestDPI1.Properties.Resources.blue_arrow_up;
            this.tSload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSload.Name = "tSload";
            this.tSload.Size = new System.Drawing.Size(101, 22);
            this.tSload.Text = "Upload from PC";
            this.tSload.Click += new System.EventHandler(this.tSCargar_Click);
            // 
            // uscNetDistribution2
            // 
            this.uscNetDistribution2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uscNetDistribution2.Location = new System.Drawing.Point(3, 3);
            this.uscNetDistribution2.MChannel = DPI1.ChannelNum.One;
            this.uscNetDistribution2.Name = "uscNetDistribution2";
            this.uscNetDistribution2.Size = new System.Drawing.Size(997, 405);
            this.uscNetDistribution2.TabIndex = 0;
            this.uscNetDistribution2.Node_Clicked += new System.EventHandler(this.uscNetDistribution2_Node_Clicked);
            // 
            // uscActions1
            // 
            this.uscActions1.Location = new System.Drawing.Point(294, 0);
            this.uscActions1.MChannel = DPI1.ChannelNum.Two;
            this.uscActions1.MSelectedNode = null;
            this.uscActions1.Name = "uscActions1";
            this.uscActions1.Size = new System.Drawing.Size(295, 387);
            this.uscActions1.TabIndex = 72;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 544);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.TSTop);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TSBottom);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fMain";
            this.Text = "DPI1";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.fMain_HelpButtonClicked);
            this.TSTop.ResumeLayout(false);
            this.TSTop.PerformLayout();
            this.TSBottom.ResumeLayout(false);
            this.TSBottom.PerformLayout();
            this.pTools.ResumeLayout(false);
            this.gbMaintenance.ResumeLayout(false);
            this.gbMaintenance.PerformLayout();
            this.gbTransmit.ResumeLayout(false);
            this.gbTransmit.PerformLayout();
            this.gbNode.ResumeLayout(false);
            this.gbNode.PerformLayout();
            this.gbOther.ResumeLayout(false);
            this.gbOther.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            this.tabCom.ResumeLayout(false);
            this.tabEvents.ResumeLayout(false);
            this.spEvents.Panel1.ResumeLayout(false);
            this.spEvents.Panel1.PerformLayout();
            this.spEvents.Panel2.ResumeLayout(false);
            this.spEvents.Panel2.PerformLayout();
            this.spEvents.ResumeLayout(false);
            this.tabRxKeypress.ResumeLayout(false);
            this.tabRxKeypress.PerformLayout();
            this.tabRxAlarms.ResumeLayout(false);
            this.tabRxAlarms.PerformLayout();
            this.tabSerialBuffer.ResumeLayout(false);
            this.tabSerialBuffer.PerformLayout();
            this.tab0.ResumeLayout(false);
            this.tab2.ResumeLayout(false);
            this.tab3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.gBProgramNodes.ResumeLayout(false);
            this.gBProgramNodes.PerformLayout();
            this.gBFirstTimeSetup.ResumeLayout(false);
            this.gBFirstTimeSetup.PerformLayout();
            this.gBSelectChannel.ResumeLayout(false);
            this.gBSelectChannel.PerformLayout();
            this.gBChangeNodeID.ResumeLayout(false);
            this.gBChangeNodeID.PerformLayout();
            this.tSAdv.ResumeLayout(false);
            this.tSAdv.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TSTop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel lPort;
        private System.Windows.Forms.ToolStripTextBox tsPort;
        private System.Windows.Forms.ToolStripButton bConnect;
        private System.Windows.Forms.ToolStripButton bDisconnect;
        private System.Windows.Forms.StatusStrip TSBottom;
        private System.Windows.Forms.ToolStripStatusLabel tSConnected;
        private System.Windows.Forms.ToolStripStatusLabel tSDisConnected;
        private System.Windows.Forms.ToolStripStatusLabel TSError;
        private System.Windows.Forms.ToolStripTextBox tsIP;
        private System.Windows.Forms.PropertyGrid pgGrid;
        private System.Windows.Forms.Panel pTools;
        private System.Windows.Forms.Button bOpenSessions;
        private System.Windows.Forms.Button bNetDistribution;
        private System.Windows.Forms.Button bSerial;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tSRefresh;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ChannelComboBox;
        private System.Windows.Forms.CheckBox cBoxBroadcast;
        private System.Windows.Forms.ComboBox NodeComboBox;
        private System.Windows.Forms.TextBox txtOpenSessions;
        private System.Windows.Forms.Button bSendAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblDLLVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbOther;
        private System.Windows.Forms.GroupBox gbNode;
        private System.Windows.Forms.GroupBox gbTransmit;
        private System.Windows.Forms.GroupBox gbMaintenance;
        private System.Windows.Forms.Timer timerCheckData;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tab3;
        private System.Windows.Forms.TabPage tab1;
        private System.Windows.Forms.TabPage tab2;
        private System.Windows.Forms.TabPage tab0;
        private System.Windows.Forms.ToolStripStatusLabel tsTime;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.CheckBox ckbTestMode;
        private System.Windows.Forms.TextBox txtSendAll;
        private System.Windows.Forms.SaveFileDialog saveFileDlg;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.ToolStrip tSAdv;
        private System.Windows.Forms.ToolStripButton tSload;
        private System.Windows.Forms.ToolStripButton tSSave;
        private System.Windows.Forms.ToolStripStatusLabel tSStatus;
        private System.Windows.Forms.GroupBox gBFirstTimeSetup;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtIDStart;
        private System.Windows.Forms.Button bFinish;
        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.ComboBox cmboBoxChannel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button bResetChannel;
        private System.Windows.Forms.TabControl tabCom;
        private System.Windows.Forms.TabPage tabEvents;
        private System.Windows.Forms.SplitContainer spEvents;
        private System.Windows.Forms.TextBox txtEvents;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bClrEvents;
        private System.Windows.Forms.Button bClrRxBuffer;
        private System.Windows.Forms.TextBox txtRxbuffer;
        private System.Windows.Forms.Label LRxBuffer;
        private System.Windows.Forms.TabPage tabRxKeypress;
        private System.Windows.Forms.TextBox txtRxKeypressBuffer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bClrKeypress;
        private System.Windows.Forms.TabPage tabRxAlarms;
        private System.Windows.Forms.TextBox txtRxAlarms;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button bClrAlarms;
        private System.Windows.Forms.TabPage tabSerialBuffer;
        private System.Windows.Forms.TextBox txtSerialBuffer;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button bClrSerialBuffer;
        private System.Windows.Forms.GroupBox gBChangeNodeID;
        private System.Windows.Forms.TextBox txtOldID;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtNewID;
        private System.Windows.Forms.Button bProgramNodeID;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.LinkLabel electrotecsc;
        private System.Windows.Forms.GroupBox gBProgramNodes;
        private System.Windows.Forms.GroupBox gBSelectChannel;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox txtTotalNodes;
        private uscActions uscActions1;
        private uscNetDistribution uscNetDistribution2;
        private System.Windows.Forms.TextBox txtAdvice;

    }
}