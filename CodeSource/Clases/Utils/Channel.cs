﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DPI1
{
    /// <summary>
    /// DPI Channel Class, with the collection of display devices connected
    /// </summary>
   [Serializable]
   [XmlInclude(typeof(Channel))] 
    public class Channel : FrameBase
    {
        /// <summary>
        /// Channel Number
        /// </summary>
        public ChannelNum ChannelNum { get; set ; }
        /// <summary>
        /// Number of display devices in the channel
        /// </summary>
        public int NumDisplays { get; set; } 
        internal List<DP> mldp = new List<DP>();
        /// <summary>
        /// List of display devices in the channel
        /// </summary>
        public List<DP> ListOfDevices { get{return mldp;}}
        protected int RequestType { get; set;}
        public Channel(){}
        internal Channel(string _rxData)
        {
            string[] ldata = _rxData.Split(Convert.ToChar(mbSplit));
            ChannelNum = (ChannelNum)(Convert.ToInt32(ldata[1]));
            RequestType=(Convert.ToInt32(ldata[2]));
            NumDisplays = (Convert.ToInt32(ldata[3]));
            
            if (NumDisplays > 0)
            {
                mldp = new List<DP>(NumDisplays);
                string[] dp = ldata[4].Split(Convert.ToChar(mbSplitByte));
                for (int i = 1; i < dp.Length; i++)
                {
                    if (Convert.ToInt32(dp[i]) > 0)
                    {
                        switch (RequestType)
                        {
                            case 1:
                                mldp.Add(new DP(i.ToString(), (DisplayType)(Convert.ToInt32(dp[i]))));
                                break;
                            case 2:
                                mldp.Add(new DP(i.ToString(), (Status)(Convert.ToInt32(dp[i]))));
                                break;
                        }
                    }
                }
                
            }
        }
        internal Channel(string _rxData, string old)
        {
            string[] ldata = _rxData.Split(Convert.ToChar(mbSplit));
            GetChannelMapData_old(ldata[0]);
            if (NumDisplays > 0)
            {
                mldp = new List<DP>(NumDisplays);
                for (int i = 1; i < ldata.Length; i++) if (!String.IsNullOrEmpty(ldata[i])) mldp.Add(new DP(ldata[i]));
            }
        }
        internal void GetChannelMapData_old(string _data)
        {
            string[] ldata = _data.Split(Convert.ToChar(mbSplitByte));
            ChannelNum = (ChannelNum)(Encoding.ASCII.GetBytes(ldata[0])[0]);
            NumDisplays = (Convert.ToInt32(ldata[1]));
        }
        internal void AddButtonInfoDP(string _id, string _KeyNum, string _KeySymbol)
        {
            int idx = this.mldp.FindIndex((DP e) => { return e.id == _id; });
            if (idx > -1) { mldp[idx].KeySymbol = _KeySymbol; mldp[idx].keyNum = _KeyNum; }
        }
        internal void UpdateChannelInfo(Channel _channel)
        {
            this.RequestType=_channel.RequestType;
            this.NumDisplays=_channel.NumDisplays;
            foreach (DP dp in _channel.mldp)
            {
                int idx = this.mldp.FindIndex((DP e) => { return e.id == dp.id; });
                if (idx > -1) 
                {
                    switch(RequestType)
                    {
                        case 1: mldp[idx].DpType=dp.DpType; break;
                        case 2: mldp[idx].State=dp.State;   break;
                    }
                }
            }
        }
        public string[] ToStringArray()
        {
            string data = "";
            string[] alldata=null;
            int i = 0, j = 0;
            if (mldp.Count == 0 || mldp.Count%50==0) alldata = new string[(mldp.Count / 50) + 1];
            else if (mldp.Count % 50 > 0) alldata = new string[(mldp.Count / 50) + 2];
            
            foreach (DP dp in mldp) 
            {
                data = data + Convert.ToChar(mbSplit) + dp.ToStringArray(); i++;
                if(i%50==0)
                {  
                    string strChannel= Encoding.ASCII.GetString(new byte[1] { (byte)ChannelNum }) + Convert.ToChar(mbSplitByte) + "050";
                    string frame=strChannel + data;
                    alldata[j] = frame;
                    data = ""; j++;
                }            
            }
            if(data!="")
            {
                 string strChannel= Encoding.ASCII.GetString(new byte[1] { (byte)ChannelNum }) + Convert.ToChar(mbSplitByte) + (i%50).ToString("000");
                 alldata[j]=strChannel + data;
                 data = ""; j++;
            }
            if (alldata[j] == null)
            {
                string strChannel = Encoding.ASCII.GetString(new byte[1] { (byte)ChannelNum }) + Convert.ToChar(mbSplitByte) + "000";
                alldata[j] = strChannel;
            }

            
            return alldata;
        }
        public override string ToString()
        { 
            return "Channel: "+ ((ChannelNum)ChannelNum).ToString("G")+ " Displays.Count: " +NumDisplays.ToString();
        }



    }

}
