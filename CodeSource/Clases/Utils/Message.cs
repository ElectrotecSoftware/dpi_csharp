﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{

    /// <summary>
    /// Display device LED Color Enum
    /// </summary>
    public enum Color
    {
        None = unchecked((int)(0x303030)), Red = unchecked((int)(0x313030)),
        Green = unchecked((int)(0x303130)), Blue = unchecked((int)(0x303031)),
        Yellow = unchecked((int)(0x313130)), Pink = unchecked((int)(0x313031)),
        White = unchecked((int)(0x313131)), LightBlue = unchecked((int)(0x303131))
    }
    /// <summary>
    /// Message to print data to display devices (color, text, blink,...)
    /// </summary>
    public class Message:FrameBase
    {
        /// <summary>
        /// Display device Blink Enum
        /// </summary>
        public Blinker Blink { get; set; }
        /// <summary>
        /// Display device Beep Type
        /// </summary>
        public BeepType Beep { get; set; }
        /// <summary>
        /// DPI Channel Number
        /// </summary>
        public ChannelNum Channel { get; set; }
        /// <summary>
        /// Display device writte Extra byte
        /// </summary>
        public ArrowsPos Arrows { get; set; }
        /// <remarks>Display device Key Sound Enum</remarks>
        public KeySound Sound { get; set; }
        /// <summary>
        /// Display device LED Color Enum
        /// </summary>
        public Color Color { get; set; } 
        /// <summary>
        /// Display device Text
        /// </summary>
        public string Text { get; set; }
        internal byte[] ToByteArray() //Build the frame
        {
            byte[] lData= new byte[500]; 
            lData[0] = mbSplit;
            lData[1] = Convert.ToByte(Channel); //Channel
            
            lData[2] = mbSplit;
            if (String.IsNullOrEmpty(Text)) Text = "..";
            byte[] lbyte=ToByte(Text);
            lbyte.CopyTo(lData,3);
            int cont=lbyte.Length+3; //Text
            
            lData[cont]=mbSplitByte;
            lbyte= AddSplitByte(BitConverter.GetBytes((int)(this.Color)).Reverse().ToArray(),3); //Color
            lbyte.CopyTo(lData,cont+1);
            cont = cont + lbyte.Length;

            new byte[]{ Convert.ToByte(Blink),mbSplitByte, //Blink
                        Convert.ToByte(Arrows),mbSplitByte, //Eeprom
                        Convert.ToByte(Sound),mbSplitByte, //keysound
                        Convert.ToByte(Beep) //beep
                        }.CopyTo(lData,cont+1); 
            cont=cont+8;
            Array.Resize(ref lData,cont);
            return lData;
 
        }
        /// <summary>
        /// Initiallizes a new class of Message
        /// </summary>
        public Message()
        {
            Blink = Blinker.NoBlink;
            Beep= BeepType.OneBeep;
            Arrows=ArrowsPos.None;
            Channel= ChannelNum.One;
            Sound = KeySound.Disabled;
            Color = Color.None;
            Text = "ELECTROTEC";

        }
        /// <summary>
        /// Initiallizes a new class of Message with blin, channel, keysound, color, etc..
        /// </summary>
      
        public Message(Blinker _blink, BeepType _beep, ChannelNum _channel, KeySound _sound, Color _color, string _text)
            : this()
        {
            Blink = _blink;
            Beep = _beep;
            Arrows = ArrowsPos.None;
            Channel = _channel;
            Sound = _sound;
            Color = _color;
            Text = _text;
        }
        public Message(Blinker _blink, BeepType _beep, ArrowsPos _arrows, ChannelNum _channel, KeySound _sound, Color _color, string _text)
            : this(_blink,_beep,_channel,_sound,_color,_text)
        {
            Arrows = _arrows;
         
        }
    }
 
}
