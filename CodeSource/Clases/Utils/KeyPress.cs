﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{
    /// <summary>
    /// Display device Key Press class
    /// </summary>

    public class KeyPress:FrameBase
    {
        /// <summary>
        /// Display identifier
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Channel num
        /// </summary>
        public ChannelNum Channel { get; set; }
        /// <summary>
        /// Display device Key Num
        /// </summary>
        public string KeyNum { get; set; }
        /// <summary>
        /// Display device Key Symbol
        /// </summary>
        public string KeySymbol { get; set; }
        /// <summary>
        /// Received TimeStamp
        /// </summary>
        public DateTime TimeStamp { get; set; }
        public KeyPress() { }
        public KeyPress(string _id, ChannelNum _Channel, string _KeyNum, string _KeySymbol):this()
        {
            id = _id;
            Channel = _Channel;
            KeyNum = _KeyNum;
            KeySymbol = _KeySymbol;
            TimeStamp = System.DateTime.Now;
        }
        internal KeyPress(string _data):this()
        {
            string[] dataButton = _data.Split(Convert.ToChar(mbSplit));
            id = dataButton[1];
            Channel = (ChannelNum)Enum.ToObject(typeof(ChannelNum),Convert.ToInt32(dataButton[2]));

            string[] keyInfo = dataButton[3].Split(Convert.ToChar(mbSplitByte));
            KeyNum = keyInfo[1];
            KeySymbol = keyInfo[2];
            TimeStamp = System.DateTime.Now;
        }
        public override string ToString()
        {
            if (String.IsNullOrEmpty(id)) return "";
            else return TimeStamp.ToString("yyyy/MM/dd hh:mm:ss.fff") + "- Channel " + Channel + " Display " + id + "- Key: " + KeyNum.ToString() + ", " + KeySymbol;
        }
    }
}
