﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DPI1
{
    /// <summary>
    /// Display device class
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(DP))] 
    public class DP:FrameBase
    {
        /// <summary>
        /// Display device identifier
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Display device Type
        /// </summary>
        public DisplayType DpType { get; set; }
        /// <summary>
        /// Display device last Key Symbol Press
        /// </summary>
        public string KeySymbol { get; set; }
        /// <summary>
        /// Display device last Key Num Press
        /// </summary>
        public string keyNum { get; set; }
        /// <summary>
        /// Display device State
        /// </summary>
        public Status State { get ; set; }

        public DP()
        {
            id = String.Empty;
            keyNum = "0";
            KeySymbol = String.Empty;
            State=Status.Unknown;
        }
        internal DP(string _rxData):this()
        {
            DisplayData(_rxData);
        }
        public DP(string _id, DisplayType _dpType): this()
        {
            id = _id;
            DpType = _dpType;
        }
        public DP(string _id, Status _status)
            : this()
        {
            id = _id;
            State = _status;
        }
        private void DisplayData(string _data)
        {
            string[] ldata = _data.Split(Convert.ToChar(mbSplitByte));
            id = ldata[0];
            State = (Status)(Encoding.ASCII.GetBytes(ldata[1]))[0];
            DpType = (DisplayType)(Encoding.ASCII.GetBytes(ldata[2]))[0];
        }
        public string ToStringArray()
        {
            return id + Convert.ToChar(mbSplitByte) + Encoding.ASCII.GetString(new byte[1] { (byte)DpType });
        }
        public override string ToString()
        {
            return "Display: " + id + " Type: " + Enum.GetName(DpType.GetType(), (object)DpType) +" Status: " + Enum.GetName(State.GetType(), (object)State);
        }
    }
  
}
