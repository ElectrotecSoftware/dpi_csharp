﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{
    /// <summary>
    /// Byte Buffer Class
    /// </summary>
    public class ByteBuffer:FrameBase
    {
        public byte[] Buffer { get; set; }
        private string strbuffer="";
        public DateTime TimeStamp { get; set; }
        public ByteBuffer() { }
        internal ByteBuffer(byte[] _Buffer): this()
        {
            strbuffer=ToStringByte(_Buffer);
            Buffer = _Buffer;
            TimeStamp = System.DateTime.Now;
        }
        internal ByteBuffer(string _Buffer): this()
        {
            strbuffer = _Buffer;
            Buffer = ToByte(_Buffer);
            TimeStamp = System.DateTime.Now;
        }
        public override string ToString()
        {
            return TimeStamp.ToString("yyyy/MM/dd hh:mm:ss.fff") + "__" + strbuffer;
            
        }
    }
}
