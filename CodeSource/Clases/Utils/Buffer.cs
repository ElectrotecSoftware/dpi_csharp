﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace DPI1
{
    /// <summary>
    /// Buffer Class Queue, with Enqueued events
    /// </summary>

    public class BufferQueue<T> : IEnumerable<T>, IEnumerable, ICollection
    {
        private Queue<T> queue = new Queue<T>();
        public event EventHandler Enqueued;
        protected virtual void OnEnqueued()
        {
            if (Enqueued != null)
                Enqueued(this, new EventArgs());
        }
        public virtual void Enqueue(T item)
        {
            queue.Enqueue(item);
            OnEnqueued();
        }
        public int Count { get { return queue.Count; } }
        public virtual T Dequeue()
        {
            T item = queue.Dequeue();
            OnEnqueued();
            return item;

        }
        public void Clear() 
        {
            queue.Clear();
        }
        #region Miembros de IEnumerable<T>

        public IEnumerator<T> GetEnumerator()
        {
            return queue.GetEnumerator();
        }

        #endregion

        #region Miembros de IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return queue.GetEnumerator();
        }

        #endregion



        #region Miembros de ICollection

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public bool IsSynchronized
        {

            get { throw new NotImplementedException(); }
        }

        public object SyncRoot
        {

            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    } 
}
