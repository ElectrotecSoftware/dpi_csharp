﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{
    /// <summary>
    /// DPI  Relays output
    /// </summary>
    public class Relays:FrameBase
    {

        public RelayOutput Relay1 { get; set; }
        public RelayOutput Relay2 { get; set; }
        public RelayOutput Relay3 { get; set; }
        internal byte[] ToByteArray()
        {
            return new byte[] { Convert.ToByte(Relay1), mbSplitByte, Convert.ToByte(Relay2), mbSplitByte, Convert.ToByte(Relay3) };
        }
        public Relays()
        {
            Relay1 = RelayOutput.Disabled;
            Relay2 = RelayOutput.Disabled;
            Relay3 = RelayOutput.Disabled;
        }
        public Relays(RelayOutput _relay1, RelayOutput _relay2, RelayOutput _relay3):this()
        {
            Relay1 = _relay1;
            Relay2 = _relay2;
            Relay3 = _relay3;
        }


    }
}
