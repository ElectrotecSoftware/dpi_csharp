﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{
    public class Serial:FrameBase
    {
        public string id { get; set; }
        public ChannelNum Channel { get; set; }
        public string BarCode { get; set; }
        public DateTime TimeStamp { get; set; }
        public Serial(){ }
        public Serial(string _id, ChannelNum _Channel, string _BarCode): this()
        {
            id = _id;
            Channel = _Channel;
            BarCode = _BarCode;
            TimeStamp = DateTime.Now;
        }
        internal Serial(string _rxData)
        {
            _rxData = _rxData.Trim('\0');
            _rxData = _rxData.Trim('\n');
            _rxData = _rxData.Trim('\r');
            string[] ldata = _rxData.Split(Convert.ToChar(mbSplit));
            id =ldata[1];
            Channel =(ChannelNum)Convert.ToInt32(ldata[2][0]);
            BarCode = ldata[3].Trim('\0');
            TimeStamp = DateTime.Now;
        }     
        public override string ToString()
        {
            if (String.IsNullOrEmpty(id)) return "";
            else return TimeStamp.ToString("yyyy/MM/dd hh:mm:ss.fff") + "- " + id + " Channel " + Channel.ToString() + ": " + BarCode;
        }
    }
}
