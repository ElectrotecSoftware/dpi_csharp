﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{
    /// <summary>
    /// Print data to display devices (color, text, blink,...) 
    /// for the SetDisplayPrintACK(List<PrintDisplay> printDisplay) function.
    /// --Send a List of PrintDisplay.ATTENTION: MAX 25 items.
    /// </summary>
    public class PrintDisplay:FrameBase
    {
        private string mId;
        private string mCode;
        private Message mMessage;
        private Alarms mResponse;

        public string Id { get {return mId;} }
        public string Code { get{return mCode;}  }
        public Message Message { get{return mMessage;}}
        public Alarms Response {get{return mResponse;}}
        public PrintDisplay()
        {
            mId = String.Empty;
            mCode = String.Empty;
            mMessage = new Message();
            mResponse = new Alarms();
        }
        public PrintDisplay(string _id,  Message _msg)
            : this()
        {
            mId = _id;
            mMessage = _msg;
        }
        internal PrintDisplay(string _id,string _code,Message _msg):this(_id, _msg)
        {       
            mCode = _code;
        }
        internal PrintDisplay(string _id, string _code, Message _msg,Alarms _response)
            : this(_id,_code,_msg)
        {
            mResponse = _response;
        }
        internal void AddCode(string _code)
        {
            mCode = _code;
        }
        internal void AddResponse(Alarms _response)
        {
            mResponse = _response;
        }
    }
}
