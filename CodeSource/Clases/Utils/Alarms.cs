﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPI1
{
    /// <summary>
    /// DPI Alarms Class
    /// </summary>
    /// <remarks>
    /// case 1:        Abbreviation="RING BROKEN".  Description = "Comunication Ring on one channel is broken."
    ///  case 3:       Abbreviation="HARDWARE ERROR" Description = "Node detected with hardware problems."
    /// case 2:        Abbreviation="NODE IS DISCONNECTED" Description = "Node detected as disconnected."
    /// case 4:        Abbreviation="WRONG FORMAT" Description = "Frame with wrong format."
    ///  case 5:       Abbreviation="NODE NOT CONFIGURED"  Description = "Alarm when trying to send data to nodes not configured in DPI interface."
    ///  case 6:       Abbreviation="TRANSMISSION FAIL" Description = "Impossible to transmit the frame to node."
    ///   case 9:      Abbreviation = "TRANSMISSION OK";  Description = "Transmission processed to node."
    ///   case 10:    Abbreviation = "TIMEOUT";   Description = "Response out of time."
    /// </remarks>
    public class Alarms : IComparable<Alarms>, IEquatable<Alarms>

    {
        /// <summary>
        /// id display
        /// </summary>
        public string id { get; set; }
        public string Code { get; set; }
        /// <summary>
        /// Channel Number
        /// </summary>
        public ChannelNum Channel { get; set; }
        /// <summary>
        /// number alarm
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// Short Desc
        /// </summary>
        public string Abbreviation { get; set; }
        /// <summary>
        /// Long Desc
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// detected timestamp
        /// </summary>
        public DateTime TimeStamp { get; set; }
        public Alarms()
        { }

        internal Alarms(string _id, ChannelNum _Channel, string number)
            : this()
        {
            id=_id;
            Channel = _Channel;
            TimeStamp = System.DateTime.Now;
            Number = Convert.ToInt32(number);
            switch (Number)
            {
                case 1: //Asincron. Problema amb l'anell
                    Abbreviation="RING BROKEN";
                    Description = "Comunication Ring on one channel is broken.";
                    break;

                case 3:  //Asincron. Problema intern a un display -- No s'envia v1.6
                    Abbreviation="HARDWARE ERROR";
                    Description = "Node detected with hardware problems.";
                    break;
                    //
                case 2: //Asincron quan es desconecta i resposta si li preguntes. 
                    Abbreviation="NODE IS DISCONNECTED";
                    Description = "Node detected as disconnected.";
                    break;
                case 4: //Resposta si li preguntes. 
                    Abbreviation="WRONG FORMAT";
                    Description = "Frame with wrong format.";
                    break;
                case 5://Resposta si li preguntes. 
                    Abbreviation="NODE NOT CONFIGURED";
                    Description = "Alarm when trying to send data to nodes not configured in DPI interface.";
                    break;
                case 6: //Asincron quan falla l'enviament al display. 
                    Abbreviation="TRANSMISSION FAIL";
                    Description = "Impossible to transmit the frame to node.";
                    break;
                case 9: //ACK
                    Abbreviation = "TRANSMISSION OK";
                    Description = "Transmission processed to node.";
                    break;
                case 10:
                    Abbreviation = "TIMEOUT";
                    Description = "Response out of time.";
                    break;
                default:
                    Abbreviation="ACKNOWLEGMENT ALARM FAIL";
                    Description = "Not reconized Alarm.";
                    break;
            }
        }
        internal Alarms(string _id, ChannelNum _Channel, string number, string code)
            : this(_id,_Channel,number)
        {
            Code = code;           
        }
        public override string ToString()
        {
            if (String.IsNullOrEmpty(id)) return "";
            else if (!String.IsNullOrEmpty(Code)) return TimeStamp.ToString("yyyy/MM/dd hh:mm:ss.fff") + "- "+Code+"_"+ id + " channel" + Channel.ToString() + " RESULT: " + Number.ToString() + " " + Abbreviation + ", " + Description;
            else return TimeStamp.ToString("yyyy/MM/dd hh:mm:ss.fff")+"- " + id + " channel" + Channel.ToString() + " ALARM: " + Number.ToString() + " " + Abbreviation + ", " + Description;
        }
        internal void Clear()
        {
            id="";
            Code = "";
        }



        #region Miembros de IComparable<Part>

        public int CompareTo(Alarms comparePart)
        {
            if (comparePart == null)
                return 1;

            else
                return this.Code.CompareTo(comparePart.Code);

        }

        #endregion

        #region Miembros de IEquatable<Part>

        public bool Equals(Alarms other)
        {
            if (other == null) return false;
            return (this.Code.Equals(other.Code));

        }

        #endregion
    }
}
