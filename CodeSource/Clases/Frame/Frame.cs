﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using DPI1;

namespace DPI1
{
    [Serializable]
    internal class Frame:FrameBase
    {
        private byte[]      mData = null;
        private byte        mbIdentifier = 0x31;
        private string      last_rx = "";
        private string      mVersion = "";
        private int         mOpenSessions = 0;
        private Alarms      mLastAlarm = new Alarms();
        private KeyPress    mLastKeyPress = new KeyPress();
        private Serial      mLastRxSerial = new Serial();
        //public Queue<KeyPress> mKeyPressBuffer = new Queue<KeyPress>(mBufferCapacity);

        public BufferQueue<KeyPress> mKeyPressBuffer = new BufferQueue<KeyPress>();
        //public Queue<Serial> mRxSerialBuffer = new Queue<Serial>(mBufferCapacity);
        //public Queue<ByteBuffer> mRxBuffer = new Queue<ByteBuffer>(mBufferCapacity);
        //public Queue<ByteBuffer> mTxBuffer = new Queue<ByteBuffer>(mTxBufferCapacity);
        //public Queue<Alarms> mAlarmBuffer = new Queue<Alarms>(mBufferCapacity);
        public BufferQueue<Serial> mRxSerialBuffer = new BufferQueue<Serial>();
        public BufferQueue<ByteBuffer> mRxBuffer = new BufferQueue<ByteBuffer>();
        public BufferQueue<ByteBuffer> mTxBuffer = new BufferQueue<ByteBuffer>();
        public BufferQueue<Alarms> mAlarmBuffer = new BufferQueue<Alarms>();
        internal List<Alarms> mPrintACK = new List<Alarms>();
        internal List<Channel> mChannels = new List<Channel>();

        private byte[] Data { get { return mData; } }
        private byte Identifier { get { return mbIdentifier; } }
        public string Version { get { return mVersion;} }
        public int OpenSessions { get { return mOpenSessions; } }
        public Alarms Alarm { get { return mLastAlarm; } }
        public KeyPress LastKeyPress { get { return mLastKeyPress; } }
        public Serial LastRxSerial { get { return mLastRxSerial; } }
        public List<Channel> Channels{ get { return mChannels; } }

        
        public Frame()
        { 
            mbIdentifier = 0x31;
            mData       =   null;
        }
        public byte[]  GetVersion()
        {
            mbIdentifier = mbTVersion; mData = null;
            return ToBuffer(4);
        }
        public byte[] ByteArray_Version()
        {
            mbIdentifier = mbTVersion;
            mData = ToByte(mVersion);
            return ToBuffer(4 + mData.Length);
        }
        private void DecodVersion(byte[] _rx)
        {
            mVersion= Encoding.ASCII.GetString(_rx, 2,3 );
        }
        public byte[] GetOpenSessions()
        {
            mbIdentifier = mbTOpenSessions; mData = null;
            return ToBuffer(4);
        }
        private void DecodOpenSessions(byte[] _rx)
        {
            mOpenSessions = Convert.ToInt32(Encoding.ASCII.GetString(_rx, 2, 1));
        }

        private void DecodAlarm(byte[] rx)
        {
            string data = Encoding.ASCII.GetString(rx, 2, rx.Length-2);
            string[] dataAlarm = data.Split(Convert.ToChar(mbSplit));
            mLastAlarm = new Alarms(dataAlarm[0], (ChannelNum)(Convert.ToInt32(dataAlarm[1])), dataAlarm[2]);
            lock (mAlarmBuffer)
            {
                while (mAlarmBuffer.Count > mBufferCapacity) mAlarmBuffer.Dequeue();
                mAlarmBuffer.Enqueue(mLastAlarm); 
            }
        }
        public byte[] SetRelay(Relays _relay)
        {
            mbIdentifier = mbTRelayOutput;  mData = _relay.ToByteArray();
            return ToBuffer(mData.Length+4);
        }
        public byte[] PrintDataDisplay(string _id,Message _msg)
        {
            mbIdentifier = mbTDisplayPrint;
            mData = GetDisplayPrint(_id,_msg);
            return ToBuffer(mData.Length + 4);
        }
        public byte[] PrintDataDisplayACK(string _code, string _id, Message _msg)
        {
            mbIdentifier = mbTDisplayPrintACK;
            _code = _code + ToStringByte(new byte[] { mbSplit });
            mData = GetDisplayPrintACK(_code, _id, _msg);
            return ToBuffer(mData.Length + 4);
        }
        public byte[] GetNetDistribution()
        {
            mbIdentifier = mbTNetDistribution; mData = null;
            return ToBuffer(4);
        }
        private void DecodNetDistribution(byte[] rx)
        {
            mChannels.Clear();
            foreach (string lchannel in GetChannels(Encoding.ASCII.GetString(rx, 2, rx.Length - 2)))
                mChannels.Add(new Channel(lchannel,"old"));

        }
       
        private void DecodNetDistribution_unused(byte[] rx)
        {
            Channel lChannel = new Channel(ToStringByte(rx));
            AddChannelInfo(lChannel);      
        }
        internal byte[][] SetNetDistribution()
        {
            mbIdentifier = mbTSetDistribution;
            string data = "";  int i = 0;
             byte[][] alldata=null;
            if(mChannels.Count==1) alldata=new byte[mChannels[0].ToStringArray().Length][];
            else alldata = new byte[mChannels[0].ToStringArray().Length + mChannels[1].ToStringArray().Length][];

            foreach (Channel channel in mChannels)
            {
                    foreach(string framepart in channel.ToStringArray()){
                    mData=ToByte(framepart);
                    alldata[i]=ToBuffer(4 + mData.Length);
                    i++;
                }
            }
            return alldata;
        }
        public void AddChannelInfo(Channel _Channel)
        {
            int idx = mChannels.FindIndex((Channel e) => { return e.ChannelNum == _Channel.ChannelNum; });
            if (idx > -1) { Channels[idx].UpdateChannelInfo(_Channel);}
            else mChannels.Add(_Channel);
        }
       
        private void DecodKeyPress(byte[] rx)
        {

           mLastKeyPress=new KeyPress(ToStringByte(rx));
           SetDataKeyPress(mLastKeyPress.Channel, mLastKeyPress.id, mLastKeyPress.KeyNum, mLastKeyPress.KeySymbol);
           lock (mKeyPressBuffer)
           {
               while (mKeyPressBuffer.Count > mBufferCapacity) mKeyPressBuffer.Dequeue();
               mKeyPressBuffer.Enqueue(mLastKeyPress); 
           }
        }
        private void DecodSerialCode(byte[] rx)
        {
            mLastRxSerial = new Serial(ToStringByte(rx));
            lock (mRxSerialBuffer)
            {
                while (mRxSerialBuffer.Count > mBufferCapacity) mRxSerialBuffer.Dequeue();
                mRxSerialBuffer.Enqueue(mLastRxSerial);
            }
        }
        private void SetDataKeyPress(ChannelNum _channel,string _id, string _KeyNum,string _KeySymbol)  
        {
            if (mChannels.Count >= (int)_channel)
            { 
                if(mChannels[(int)_channel-1].NumDisplays>0) 
                    mChannels[(int)_channel-1].AddButtonInfoDP(_id,_KeyNum,_KeySymbol);
            }
        }

        private void DecodDisplayPrintACK(byte[] rx)
        {
            //DecodAlarm();
            string data = Encoding.ASCII.GetString(rx, 2, rx.Length - 2);
            string[] dataAlarm = data.Split(Convert.ToChar(mbSplit));
            mLastAlarm = new Alarms(dataAlarm[1], (ChannelNum)(Convert.ToInt32(dataAlarm[2])), dataAlarm[3],dataAlarm[0]);
            lock (mPrintACK)
            {
                while (mPrintACK.Count > mBufferCapacity) mPrintACK.Remove(mPrintACK.First());
                mPrintACK.Add(mLastAlarm);
            }
        }
        private byte[] GetDisplayPrint(string _idDisplay,Message _msg)
        {
            var x=ToByte(_idDisplay);
            var y=_msg.ToByteArray();
            var z=new byte[x.Length+y.Length];
            x.CopyTo(z,0);
            y.CopyTo(z,x.Length);
            return z;
         }
        private byte[] GetDisplayPrintACK(string _code,string _idDisplay, Message _msg)
        {
            var u = ToByte(_code);
            var x = ToByte(_idDisplay);
            var y = _msg.ToByteArray();
            var z = new byte[u.Length + x.Length + y.Length + 1];
            u.CopyTo(z, 0);
            x.CopyTo(z, u.Length);
            y.CopyTo(z, u.Length+x.Length);
            return z;
        }
        internal byte[] SetProgramNodeID(byte _channel,string NodeOld, string NodeNew)
        {
            mbIdentifier = mbTProgramNodeID;
            var u = new byte[]{_channel};
            var n = new byte[]{ mbSplitByte};
            var x = ToByte(NodeOld);
            var t = new byte[]{mbSplit};
            var y = ToByte(NodeNew);
            var z = new byte[u.Length + x.Length + y.Length + 3];
            u.CopyTo(z, 0);
            n.CopyTo(z, u.Length);
            x.CopyTo(z, u.Length+ n.Length);
            t.CopyTo(z, u.Length + n.Length+x.Length);
            y.CopyTo(z, u.Length + n.Length + x.Length + t.Length);
            mData = z;
            return ToBuffer(4+mData.Length);
        }

        internal byte[] SetProgramNodeInc(byte _channel, string StartId,byte type)
        {
            mbIdentifier = mbTProgramNodeInc;
            var u = new byte[] { _channel };
            var n = new byte[] { mbSplitByte };
            var x = ToByte(StartId);
            var t = new byte[] { mbSplit };
            var y = new byte[] { type };
            var z = new byte[u.Length + x.Length + y.Length + 3];
            u.CopyTo(z, 0);
            n.CopyTo(z, u.Length);
            x.CopyTo(z, u.Length + n.Length);
            t.CopyTo(z, u.Length + n.Length + x.Length);
            y.CopyTo(z, u.Length + n.Length + x.Length + t.Length);
            mData = z;
            return ToBuffer(4 + mData.Length);
        }

        private string[] GetChannels(string _data)
        {
            string strChannel = ToStringByte(new byte[] { mbSplit, mbSplit });
            if (_data.Contains(strChannel)){ _data = _data.Replace(strChannel, "^"); //substituim el separador de 2 caracters per un altre
                  return  _data.Split("^".ToCharArray());}
            else  return null;
        }
 
        private byte[] ToBuffer(int _nbytes)
        {
            int i=0;
            byte[] buffer=new byte[_nbytes];

            //TELEGRAM STRUCTURE
            buffer[0]=mbStart;          //START
            buffer[1]=mbIdentifier;     //ID
            buffer[2]=mbSplit;          //SPLITTER
            if (this.Data!=null)        //DATA
                for (i = 0; i < Data.Length; i++) buffer[i + 3] = Data[i];
            buffer[i+3]=mbEnd;          //END
            return buffer;
        }

        private void DecodTelegram(string _telegram)
        {
             byte[] _rx = ToByte(_telegram);
 
             byte mbIdentifier = _rx[0];
             byte bSeparador = _rx[1];
             if (bSeparador == mbSplit)       //Valid  telegram structure
             {
                 switch (mbIdentifier) //Byte identifier
                 {
                     case mbTVersion: DecodVersion(_rx);  break;   //Version
                     case mbTOpenSessions: DecodOpenSessions(_rx); break;     //Open session
                     case mbTAlarm: DecodAlarm(_rx); break;     //Alarm reception
                     case mbTKeyPress: DecodKeyPress(_rx); break;    //KeyPress
                     case mbTNetDistribution: DecodNetDistribution(_rx); break;       //NetworkDistribution
                     case mbTSerial: DecodSerialCode(_rx);  break;
                     case mbTDisplayPrintACK: DecodDisplayPrintACK(_rx); break; //DisplayPrintACK
                     case mbTRelayOutput:
                     case mbTSetMem:
                     case mbTGetMem: break;
                 }
             }
             else  //String identifier
             {
                 string msIdentifier = _telegram.Split(Convert.ToChar(mbSplit))[0];
                 switch (msIdentifier)
                 {
                     case msTSerialCode: DecodSerialCode(_rx); break; //BarCodeReader
                 }
             }
    
        }

    
        public void FromBuffer(byte[] _rx)
        {
            string _data, str_rx = ToStringByte(_rx);
            string strEnd = ToStringByte(new byte[] { mbEnd });
            string strStart = ToStringByte(new byte[] {mbStart });
           
            _data = last_rx + str_rx;

            if (_data.Contains(strStart))
            {
                string[] _subdata = _data.Split(strStart.ToCharArray());
                foreach (string frame in _subdata)
                {
                    if (frame.Contains(strEnd))
                    {
                        string[] data = frame.Split(strEnd.ToCharArray());
                        foreach (string message in data)
                        {
                            if (!String.IsNullOrEmpty(message))
                            {
                                DecodTelegram(message);
                                last_rx = "";
                            }
                            //COMPLETE MESSAGE, between start and end
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(frame)) last_rx = strStart+frame;
                    }
                }
            }
            else if (_data.Contains(strEnd)) { last_rx = "";}
            else { last_rx = _data;}

            if (!String.IsNullOrEmpty(str_rx))
            {
                lock (mRxBuffer)
                {
                    while (mRxBuffer.Count >= mBufferCapacity) mRxBuffer.Dequeue();
                    mRxBuffer.Enqueue(new ByteBuffer(str_rx));
                }
            }
          
        }


        
    }
}
