﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DPI1
{
    /// <summary>
    /// Display device Blink Enum
    /// </summary>
   [Serializable]
    public enum Blinker { NoBlink = 0x30, Fast = 0x31, Normal = 0x32, Slow = 0x34 };
    /// <summary>
    /// Display device Beep Type
    /// </summary>
   [Serializable]
   public enum BeepType { NoBeep = 0x30, OneBeep = 0x31, DoubleShortBeep = 0x32, DoubleLongBeep = 0x33 };
    /// <summary>
    /// DPI Channel Number
    /// </summary>
   [Serializable]
   public enum ChannelNum { [XmlEnum("1")] One = 0x31,[XmlEnum("2")] Two = 0x32 };

     /// <summary>
    /// DPI Arrows Position
    /// </summary>
   [Serializable]
   public enum ArrowsPos {None=0x30, LeftUp=0x31, RightUp = 0x32,LeftDown=0x33,RightDown=0x34,Left=0x35,Right=0x36,Up=0x37,Down=0x38 };
    /// <summary> 
    /// Display device Key Sound Enum
    /// </summary>
   [Serializable]
    public enum KeySound    { Enabled = 0x31, Disabled = 0x30 };
    /// <summary>
    /// DPI Relay output
    /// </summary>
   [Serializable]
    public enum RelayOutput { Enabled = 0x31, Disabled = 0x30 };
    /// <summary>
    /// Display device Types
    /// </summary>
   [Serializable]
    public enum DisplayType { Unknown=0x30, DPA1 = 0x31, DPAZ1 = 0x32, DPM1=0x33, DPMZ1=0x34,LC1=0x35,LCI=0x36, DPW1=0x37, DPA2=0x38 };
    /// <summary>
    /// Display device Status Enum
    /// </summary>
   [Serializable]
    public enum Status      { Unknown=0x30,Init=0x31,Local=0x32, Normal=0x33, Error=0x34, Program=0x35};
  
   [Serializable]
    public enum NetDistributionRequestType { DeviceType=0x31,Status=0x32};
  
   [Serializable]
   public enum ProgramNodeInc { Init = 0x31, End = 0x32 };

    public class FrameBase
    {

        //Telegram structure
        protected const byte mbStart = 0x02;
        protected const byte mbSplit = 0x05;
        protected const byte mbSplitByte = 0x2c;
        protected const byte mbEnd = 0x03;

        //Telegram types
        protected const byte mbTVersion          = 0x31;
        protected const byte mbTOpenSessions     = 0x32;
        protected const byte mbTAlarm            = 0x33;
        protected const byte mbTDisplayPrint     = 0x34;
        protected const byte mbTRelayOutput      = 0x35;
        protected const byte mbTKeyPress         = 0x36;
        protected const byte mbTSetMem           = 0x37;
        protected const byte mbTGetMem           = 0x38;
        protected const byte mbTNetDistribution  = 0x39;
        protected const byte mbTSerial           = 0x40;
        protected const byte mbTSetDistribution  = 0x3a; 
        protected const byte mbTProgramNodeID    = 0x3b;
        protected const byte mbTProgramNodeInc   = 0x3c;
        protected const byte mbTDisplayPrintACK         = 0x42;
        protected const string msTSerialCode            = "10";

        protected const int mBufferCapacity     = 10;
        public const int mTxBufferCapacity = 2;
        static public byte[] ToByte(string _message)
        {
            if (!String.IsNullOrEmpty(_message))
            { return System.Text.Encoding.ASCII.GetBytes(_message);
            }else return null;
        }
        protected string ToStringByte(byte[] _bytes)
        {
            return Encoding.ASCII.GetString(_bytes, 0, _bytes.Length);
        }
        protected byte[] SplitByte(byte[] _base, int _startIdx, byte _separator)
        {
            int i=0;
            bool find=false;
            byte[] lData=new byte[10];
            while (i < _base.Length && !find)
            {
                if (_base[_startIdx + i] == _separator) find = true;
                else lData[i]=_base[_startIdx + i];
                i = i + 1;
            }
            Array.Resize(ref lData, i-1);
            return lData;
        }
        protected byte[] AddSplitByte(byte[] _base)
        {
            return AddSplitByte(_base, _base.Length);
        }
        protected byte[] AddSplitByte(byte[] _base, int _nbytes)
        {
            int dif = _base.Length - _nbytes;
            byte[] lbase = new byte[_nbytes * 2];
            for (int i = 0; i < _nbytes; i++)
            {
                lbase[i * 2] = _base[i + dif];
                lbase[(i * 2) + 1] = mbSplitByte;
            }
            return lbase;
        }
          
        public byte[] ReplaceBytes (byte[] src, string replace, string replacewith)
        {
            string hex = BitConverter.ToString(src);
            hex = hex.Replace("-", "");
            hex = hex.Replace(replace, replacewith);
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
