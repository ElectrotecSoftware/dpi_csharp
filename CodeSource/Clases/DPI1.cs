﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.ComponentModel;
using System.Net.Sockets;
using System.Timers;
using System.Threading;
using System.Net.NetworkInformation;
using System.Xml.Serialization;
using System.IO;

namespace DPI1
{   
    [Serializable]
    public class DPI1 : IDisposable
    {
        #region Private Definitions
        private DateTime mLastscan;
        private DateTime mLastKeepAlive;
        private DateTime mLastTransmit; 
        private int mScantime = 5;
        private int mKeepAlive =3000;
        private const int mMINScantime = 6;
        private const int mMAXTotalDisplays= 25;
        private ClientTCP mTcp=null;
        private int mCntTimeout=0;
        [XmlIgnore]private Queue<ByteBuffer> mTxBuffer = new Queue<ByteBuffer>();
        private Frame mFrame=new Frame();
        private Relays mRelay= new Relays();
        private Message mMessage= new Message();
        private System.Timers.Timer mMonitor = new System.Timers.Timer();
        #endregion

        #region PROPERTIES
        /// <summary>
        /// KeyPress buffer count from the display devices
        /// </summary>
        [Category("KeyPress"), Description("KeyPress buffer count"), XmlIgnore]
        private int KeyPressBuffer_Count
        {
            get { return mFrame.mKeyPressBuffer.Count; }
        }
        /// <summary>
        /// KeyPress buffer queue from the display devices
        /// </summary>
        [Category("KeyPress"), Description("KeyPress buffer"), XmlIgnore]
        public BufferQueue<KeyPress> KeyPressBuffer
        {
            get { return mFrame.mKeyPressBuffer; }
        }
        /// <summary>
        /// Last KeyPress from the display devices
        /// </summary>
        [Category("KeyPress"), Description("Last KeyPress"), XmlIgnore]
        public KeyPress LastKeyPress
        {
            get { return mFrame.LastKeyPress; }        
        }
        /// <summary>
        /// Rx buffer queue from the DPI device
        /// </summary>
        [Category("Data"), Description("Rx byte buffer"), XmlIgnore]
        public BufferQueue<ByteBuffer> rxBuffer
        {
            get { return mFrame.mRxBuffer; }
        }
        /// <summary>
        /// Tx buffer queue from the DPI device
        /// </summary>
        [Category("Data"), Description("Tx byte buffer"), XmlIgnore]
        internal BufferQueue<ByteBuffer> TxBuffer
        {
            get { return mFrame.mTxBuffer; }
        }
        /// <summary>
        /// Serial byte buffer queue
        /// </summary>
        [Category("Data"), Description("Serial byte buffer"), XmlIgnore]
        public BufferQueue<Serial> RxSerialBuffer 
        {
            get { return mFrame.mRxSerialBuffer; }
        }
        /// <summary>
        /// Network distribution Collection (list of Channels)
        /// </summary>
        [Category("_General"), Description("Network distribution")]
        public List<Channel> NetDistribution
        {
            get { return mFrame.Channels; }
        }
        /// <summary>
        /// DPI relays
        /// </summary>
        [Category("Functions"), Description("DPI relays"), XmlIgnore]
        public Relays Relays
        {
            get { return mRelay; }
            set { try { mRelay = value; } catch { mRelay = new Relays(); } }
        }
        /// <summary>
        /// DPI communication port
        /// </summary>
        [Category("_Configuration"), Description("Port number")]
        public int? Port
        {
            get { return mTcp.Port; }
            set { if (value != null) mTcp.Port = value.Value; }
        }
        /// <summary>
        /// DPI communication Address
        /// </summary>
        [Category("_Configuration"), Description("Ip Address")]
        public string IppAdress
        {
            get { return mTcp.IpAddress.ToString(); }
            set { if (!String.IsNullOrEmpty(value)){ mTcp.IpAddress= IPAddress.Parse(value);}}
        }
        /// <summary>
        /// Message to print data to display devices (color, text, blink,...)
        /// </summary>
        [Category("Functions"), Description("Print message"), XmlIgnore]
        public Message Message
        {
            get { return mMessage; }
            set { try { mMessage = value; } catch { mMessage = new Message(); } }
        }
        /// <summary>
        /// DPI firmware version
        /// </summary>
        [Category("_General"), Description("DPI firmware version")]
        public string Version
        {
            get { return (String.IsNullOrEmpty(mFrame.Version))? "0.0" : mFrame.Version; }
        }
        /// <summary>
        /// DPI firmware version
        /// </summary>
        [Category("_General"), Description("Active sessions")]
        public int OpenSessions
        {
            get { return mFrame.OpenSessions; }
        }
        /// <summary>
        ///Last DPI Alarm reception
        /// </summary>
        [Category("Error"), Description("Last DPI Alarm reception"), XmlIgnore]
        public string LastAlarm
        {
            get { return mFrame.Alarm.ToString(); }
        }
        /// <summary>
        /// Alarm buffer queue
        /// </summary>
        [Category("Error"), Description("DPI Alarm Buffer"), XmlIgnore]
        public BufferQueue<Alarms> AlarmBuffer
        {
            get { return mFrame.mAlarmBuffer; }
        }
        /// <summary>
        ///Last Read time stamp
        /// </summary>
        [Category("Comunications"), Description("Last Read time stamp")]
        public string LastScan
        {
            get { return mLastscan.ToString("yyyy/MM/dd hh:mm:ss.fff"); } 
        }
        /// <summary>
        ///Last Send Keep Alive
        /// </summary>
        [Category("Comunications"), Description("Last Send KeepAlive")]
        public string LastKeepAlive
        {
            get { return mLastKeepAlive.ToString("yyyy/MM/dd hh:mm:ss.fff"); }
        }
        /// <summary>
        ///Scan time cicle miliseconds
        /// </summary>
        [Category("Comunications"), Description("Scan time cicle miliseconds")]
        public int ScanTime
        {
            get { return mScantime; }
            set
            {
                if (value < mKeepAlive && value > mMINScantime) { mScantime = value; }
                else mScantime = mMINScantime;
            }
        }
        /// <summary>
        ///Send/Receive communications timeout
        /// </summary>
        [Category("Comunications"), Description("Send/Receive timeout")]
        public int Timeout
        {
            get { return mTcp.Timeout; }
            set { mTcp.Timeout = value; }
        }
        /// <summary>
        /// True if there is an error with comunications
        /// </summary>
        [Category("Error"), Description("There is an error with comunications")]
        public bool Error { get { return mTcp.error; } }
        /// <summary>
        /// Communications error description
        /// </summary>
        [Category("Error"), Description("Communication error description")]
        public string StrError { get { return mTcp.log; } }
        /// <summary>
        /// Communications with DPI are open or closed
        /// </summary>
        [Category("Comunications"), Description("DPI is connected")]
        public bool IsOpen { get { try { return mTcp.s.Connected;}catch{return false;}; } }
        #endregion

        #region PUBLIC FUNCTIONS & EVENTS
        /// <summary>
        /// Initializes a new instance of the DPI1 class
        /// </summary>
        public DPI1() 
        {
            mTcp = new ClientTCP(16, "192.168.0.200");
            mMonitor.Interval = mScantime;
            
        }
        /// <summary>
        /// Initializes a new instance of the DPI1 class with the specified ip, port and scantime
        /// </summary>
        public DPI1(string _ip, int _port, int _scantime):this()
        {

            mTcp.Port = _port;
            mTcp.IpAddress =IPAddress.Parse(_ip);
            mScantime = _scantime;
            mMonitor.Interval = mScantime;
               
        }

        /// <summary>
        /// Load Configutarion and Node from a backup file.
        /// SET Network Distribution
        /// IMPORTANT: This frame stores contend to microSD. The repeated use of this function can kill the stored content on microSD and cause malfunction of DPI interface. 
        /// </summary>
        public void Load(string path)
        {
            XmlSerializer XmlNodes = new XmlSerializer(typeof(DPI1));
            if (File.Exists(path))
            {
                try
                {
                    using (StreamReader reader = new StreamReader(File.OpenRead(path)))
                    {

                        DPI1 newDPI1= ((DPI1)XmlNodes.Deserialize(reader));
                        this.Port = newDPI1.Port;
                        this.IppAdress = newDPI1.IppAdress;
                        this.ScanTime = newDPI1.ScanTime;
                        this.Timeout = newDPI1.Timeout;
                        this.mFrame.mChannels = newDPI1.mFrame.mChannels;    
                    }
                    foreach (byte[] frame in this.mFrame.SetNetDistribution())
                    { SendMessage(frame); Thread.Sleep(500); }
                }
                catch (Exception e) { mTcp.error = true; mTcp.log = e.Message; }
            }
        }
        /// <summary>
        /// Save Configutarion and Nodes to a backup file
        /// </summary>
        public void Save(string path)
        {
            XmlSerializer XmlNodes = new XmlSerializer(typeof(DPI1));

            if (!Directory.Exists(Path.GetDirectoryName(path))) Directory.CreateDirectory(Path.GetDirectoryName(path));
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    XmlNodes.Serialize(writer, this);
                    writer.Close();
                }
            }
            catch (Exception e) { mTcp.error = true; mTcp.log = e.Message; }
        }

        /// <summary>
        /// Occurs when the connection status changes. Check the property IsOpen to get the status.
        /// </summary>
        public event EventHandler ConnectedStatusChanged;
        /// <summary>
        /// Occurs when the PrintDisplayACK receives data. The object source  parameter includes the response  and the messages.
        /// </summary>
        public event EventHandler PrintMessageACK;

        /// <summary>
        /// Set the realy output of the DPI device
        /// </summary>
        public void SetRelayOutput()
        {
            SendMessage(mFrame.SetRelay(mRelay));
        }
        /// <summary>
        /// Get the DPI Version
        /// </summary>
        public void GetVersion()
        {
            SendMessage(mFrame.GetVersion());
        }
        /// <summary>
        /// Get the Open Sessions of the system
        /// </summary>
        public void GetOpenSessions()
        {
            SendMessage(mFrame.GetOpenSessions());
        }
        /// <summary>
        /// Send an instance of the class Message to the specified display device  identifier d of the system
        /// </summary>
        public void SetDisplayPrint(string id)
        {
            SendMessage(mFrame.PrintDataDisplay(id, mMessage));
        }

        /// <summary>
        /// Send a List of PrintDisplay.ATTENTION: MAX 25 items.
        /// </summary>
        /// <remarks>
        /// If there is no error in the reception returns true. 
        /// If there is an error in the reception return false and throws the PrintMessage Event
        /// </remarks>
        /// <returns>Error (true or false)</returns>
        public void SetDisplayPrintACK(List<PrintDisplay> printDisplay)
        {
            try
            {
                int max=(printDisplay.Count<mMAXTotalDisplays)?printDisplay.Count:mMAXTotalDisplays ;
                for (int i = 1; i <= max; i++)
                {
                    printDisplay[i - 1].AddCode(i.ToString("000"));
                    SendMessage(mFrame.PrintDataDisplayACK(printDisplay[i - 1].Code, printDisplay[i - 1].Id, printDisplay[i - 1].Message));
                }
                mLastTransmit = DateTime.Now;
                do
                {
                    if (mFrame.mPrintACK.Count == max) break; //Responses received
                    Thread.Sleep(1);
                } while (DateTime.Now.Subtract(mLastTransmit).TotalMilliseconds <= (this.Timeout));
                mFrame.mPrintACK.Sort();
                for (int i = 1; i <= max; i++)
                {
                    int idx = mFrame.mPrintACK.FindIndex((Alarms e) => { return e.Code == printDisplay[i - 1].Code; });
                    if (idx <= -1) printDisplay[i - 1].AddResponse(new Alarms(printDisplay[i - 1].Id, printDisplay[i - 1].Message.Channel, "10", printDisplay[i - 1].Code));
                    else printDisplay[i - 1].AddResponse(mFrame.mPrintACK[idx]);
                }
                PrintMessageACK(printDisplay, new EventArgs()); 
                
            }
            catch (Exception e) { mTcp.error = true; mTcp.log = e.Message; }

            mFrame.mPrintACK.Clear();
        }

        /// <summary>
        /// Get the Net Distribution of the system
        /// </summary>
        public void GetNetDistribution()
        {
            SendMessage(mFrame.GetNetDistribution());
        }
 

        public void ProgramNodeID(ChannelNum _channel,string idOld, string idNew)
        {
            SendMessage(mFrame.SetProgramNodeID(((byte)_channel), idOld, idNew));
        }
        public void ProgramIncInit(ChannelNum _channel, string StartId)
        {
            SendMessage(mFrame.SetProgramNodeInc(((byte)_channel), StartId, (byte)ProgramNodeInc.Init));
        }
        public void ProgramIncEnd()
        {
            SendMessage(mFrame.SetProgramNodeInc((byte)ChannelNum.One, "000", (byte)ProgramNodeInc.End));
        }

        /// <summary>
        /// Starts communications with the DPI device
        /// </summary>
        public bool Start()
        {
            try { if (mTcp.InitNet()) { InitDPI1(); } }
            catch (Exception e) { mTcp.error = true; mTcp.log = e.Message; }
            return !mTcp.error; 
        }
        /// <summary>
        /// Stops communications with the DPI device
        /// </summary>
        public bool Stop()
        {
            try
            {
                mTcp.CloseNet();
                StopDPI1();
            }
            catch (Exception e) { mTcp.error = true; mTcp.log = e.Message; }
            return !mTcp.error;
        }

        #endregion

        #region INTERNAL FUNCTIONS & EVENTS
        /// <summary>
        /// Check Receibe Data
        /// </summary>
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            try
            {
                DateTime mNow = DateTime.Now;
                if (this.IsOpen != mTcp.isopen && ConnectedStatusChanged != null) { mTcp.isopen = this.IsOpen; ConnectedStatusChanged(this.IsOpen, new EventArgs()); }
               
 
                if (mTcp.ReceiveData())
                {
                    mCntTimeout = 0; mLastKeepAlive = DateTime.Now;
                    mFrame.FromBuffer(mTcp.rxbuffer);
                  
                    
                }
                else 
                {
                    if (mNow.Subtract(mLastKeepAlive).TotalMilliseconds > mKeepAlive && this.IsOpen)
                    {
                        if (!SendPing()) mCntTimeout++; mLastKeepAlive = DateTime.Now;
                        if (mCntTimeout > 2) { mTcp.CloseNet(); throw new SocketException((int)SocketErrorCodes.ConnectionTimedOut); }
                    }

                }
               
                  

                mLastscan = DateTime.Now;
            }
            catch (Exception ex)
            {
                mTcp.error = true;
                mTcp.log = ex.Message;
                Thread.Sleep(1);
            }
        }
        /// <summary>
        /// Send TCP Message
        /// </summary>
        private void SendMessage(byte[] txbuffer)
        {
            //lock (mTxBuffer)
            //{
            //   // while (mTxBuffer.Count >= FrameBase.mTxBufferCapacity) { mTxBuffer.Dequeue(); Thread.Sleep(1); }
            //    mTxBuffer.Enqueue(new ByteBuffer(txbuffer));
            //}
            Thread.Sleep(mScantime);
            mTcp.SendCommand(txbuffer);
            //if (mTcp.SendCommand(txbuffer) && this.IsOpen) SendOK(this, new EventArgs()); ;           
        }
        /// <summary>
        /// Initialize DPI1
        /// </summary>
        internal void InitDPI1()
        {
            mCntTimeout = 0;  
            mLastscan =  mLastKeepAlive = DateTime.Now;
            mTcp.error = false;
            
            
            mMonitor.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            mMonitor.Enabled = true;         
            mMonitor.Start();
            GetNetDistribution(); GetVersion(); GetOpenSessions(); 
            
        }
        /// <summary>
        /// Stop DPI1
        /// </summary>
        internal void StopDPI1()
        {
            mFrame.Channels.Clear();
            mMonitor.Stop();
            if (mTcp.isopen != this.IsOpen && ConnectedStatusChanged != null) { mTcp.isopen = this.IsOpen; ConnectedStatusChanged(this.IsOpen, new EventArgs()); }
            mTcp.error = false;
        }
        /// <summary>
        /// Send ping to check connection status
        /// </summary>
        internal bool SendPing()
        {
            Ping pingSender = new Ping();
            PingReply reply = pingSender.Send(mTcp.IpAddress, 50, Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), new PingOptions(180, true));
            return (reply.Status == IPStatus.Success);

        }
 
        #endregion

        #region Miembros de IDisposable
        internal bool disposed = false;

        /// <summary>
        /// Disposes the resource 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)     return;

            if (disposing) { mMonitor.Close(); } disposed = true;
         }
        /// <summary>
        /// Destroys the class instance 
        /// </summary>
        ~DPI1(){Dispose(false);}
        #endregion
        

    }
}
