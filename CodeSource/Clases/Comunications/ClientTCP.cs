﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets; // For TcpClient, NetworkStream, SocketException
using System.Threading;
namespace DPI1
{
    public enum SocketErrorCodes
    {
        InterruptedFunctionCall = 10004,
        PermissionDenied = 10013,
        BadAddress = 10014,
        InvalidArgument = 10022,
        TooManyOpenFiles = 10024,
        ResourceTemporarilyUnavailable = 10035,
        OperationNowInProgress = 10036,
        OperationAlreadyInProgress = 10037,
        SocketOperationOnNonSocket = 10038,
        DestinationAddressRequired = 10039,
        MessgeTooLong = 10040,
        WrongProtocolType = 10041,
        BadProtocolOption = 10042,
        ProtocolNotSupported = 10043,
        SocketTypeNotSupported = 10044,
        OperationNotSupported = 10045,
        ProtocolFamilyNotSupported = 10046,
        AddressFamilyNotSupported = 10047,
        AddressInUse = 10048,
        AddressNotAvailable = 10049,
        NetworkIsDown = 10050,
        NetworkIsUnreachable = 10051,
        NetworkReset = 10052,
        ConnectionAborted = 10053,
        ConnectionResetByPeer = 10054,
        NoBufferSpaceAvailable = 10055,
        AlreadyConnected = 10056,
        NotConnected = 10057,
        CannotSendAfterShutdown = 10058,
        ConnectionTimedOut = 10060,
        ConnectionRefused = 10061,
        HostIsDown = 10064,
        HostUnreachable = 10065,
        TooManyProcesses = 10067,
        NetworkSubsystemIsUnavailable = 10091,
        UnsupportedVersion = 10092,
        NotInitialized = 10093,
        ShutdownInProgress = 10101,
        ClassTypeNotFound = 10109,
        HostNotFound = 11001,
        HostNotFoundTryAgain = 11002,
        NonRecoverableError = 11003,
        NoDataOfRequestedType = 11004
    }
    internal class ClientTCP
    {


        //variables de configuracion
        internal Socket s;
        private int timeout = 4000;
        internal byte[] rxbuffer = new byte[1024];
        //internal int rx_bytes = 0;
        internal int tx_bytes = 0;
        // variables de control
        internal bool isopen = false;        // socket abierto?¿?    
        internal bool error = false;         // error en la dll
        internal string log = "";       // descripcion error dll

        internal int Port {  get; set; }
        internal IPAddress IpAddress { get; set; }
        internal int Timeout              
        {
            get { return timeout; }
            set { timeout = value; }
        }




        /// <summary>
        /// Initializes a new instance of the ClientTCP class
        /// </summary>
        public ClientTCP() { }
        /// <summary>
        /// Initializes a new instance of the ClientTCP class with the specified address and port.
        /// </summary>
        public ClientTCP(int _port, string _ip)
        {
            this.IpAddress = IPAddress.Parse(_ip);
            this.Port = _port;
        }
        internal bool InitNet()
        {         
            try
            {
                this.isopen = false;
                s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                s.Ttl = 42;
                s.ReceiveTimeout = s.SendTimeout = timeout;
       
                log = "Connecting...";
                s.Connect(this.IpAddress, this.Port);
                error = false;        
                log = "Connected";
            }
            catch {error = true;  log = "Imposible to connect";  }
            return !error;
        }

        internal bool CloseNet()
        {
            try
            {
                log = "Trying to stop...";
                s.Close();
                error = false;
                log = "Connection closed";
            }
            catch { error = true; log = "Imposible to disconnect"; }
            return !error;
        }

        
        internal bool SendCommand(byte[] frame)
        {
            try
            {
                s.Blocking = true;
                tx_bytes=s.Send(frame, frame.Length, SocketFlags.None);
                s.Blocking = false;
                ResetError();
                return tx_bytes>0;
            }
            catch (SocketException e)
            {
                if (e.NativeErrorCode != (int)SocketErrorCodes.ResourceTemporarilyUnavailable)
                {
                    this.error = true;
                    this.log = e.Message;
                }
                s.Blocking = false;
                Thread.Sleep(1);
                return false;  
            }
            catch(Exception e)
            {       this.error = true;
                    this.log = e.Message;
                    return false;
            }
        }

        internal bool ReceiveData()
        {
            try
            {
                Array.Clear(rxbuffer, 0, rxbuffer.Length);
                int rx_bytes = 0;
                s.Blocking = true;
                if (s.Available > 0) rx_bytes = s.Receive(rxbuffer, SocketFlags.None);
                else rx_bytes = 0;
                s.Blocking = false;
                ResetError();

                return rx_bytes > 0;
            }
            catch (SocketException e)
            {
                if (e.NativeErrorCode != (int)SocketErrorCodes.ResourceTemporarilyUnavailable)
                {
                    this.error = true;
                    this.log = e.Message;
                }
                Thread.Sleep(1);
            }

            s.Blocking = false;
            return false;  
          
        }
        private void ResetError()
        {
            this.error =false;
            this.log = "";
        }
   


    }
}
