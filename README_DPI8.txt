--------------------------------------------------------------------------------------------------------------
DPI8.dll README
**************************************************************************************************************
**Last Modified 28/10/2015
**************************************************************************************************************
Cambios versi�n DLL:
- Funcion Load xml- SetNetworkDistribution
DPITester:
- Funcion Upload xml
- Test Mode mas rapido, no hay sleep entre paquetes de tramas
**************************************************************************************************************
**Last Modified 16/09/2015
**************************************************************************************************************
Cambios versi�n DLL:
- A�adido DPA2 25/06/2015
- A�adido  --> if (mChannels.Count >= (int)_channel) en SetDataKeyPress
DPITester:
- Cuando clicas un nodo de la lista de nodos puedes realizar acciones
- Funcion Reset Channel y programNode funcionan correctamente
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
DPI7.dll README
**************************************************************************************************************
**Last Modified 19/05/2015
**************************************************************************************************************
Cambios versi�n DLL:
- Funcion Save - Guardar configuracion en xml
- Funcion Load - Escribir Lista de Nodos en el DPI - SET Network distribution
- Funcion Program Node Id
DPITester:
- Cambio dise�o interfaz de testeo
- A�adidos tres niveles de acceso (Modificar AccessLevel en TestDPI1.exe.config)
- Funcion DownLoad & Save
- Funcion Upload
- Funcion Program Node Id
--------------------------------------------------------------------------------------------------------------
DPI6.dll README
**************************************************************************************************************
**Last Modified 19/09/2014
**************************************************************************************************************
Cambios versi�n DLL, DPI-2C:
- Reconexiones de la tarjeta de red, se detecta que la tarjeta se desconecta. 
A�adido KeepAlive interno para detectar perdidas de comunicaciones.
- DPI.isopen se actualiza conforme la propiedad connected del socket.
- Fotoc�lula, cambio firmware DPI1 para detectar mensaje. 
- Confirmaci�n de que el mensaje se ha mandado. - Evento Send OK
- Canal dos manda mensaje de error sin estar conectado el lazo.
- Evento al cambio de estado de la conexion.
Pendiente:
- Evento para confirmar envio. por la interface
- Evento de mensaje enviado y procesado
